# echo "cleanup Ds2KKPiPi0Alg Ds2KKPiPi0-master in /afs/ihep.ac.cn/users/h/hancj/besfs/boss-7.0.3/workarea"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /afs/ihep.ac.cn/bes3/offline/ExternalLib/SLC6/contrib/CMT/v1r25
endif
source ${CMTROOT}/mgr/setup.csh
set cmtDs2KKPiPi0Algtempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set cmtDs2KKPiPi0Algtempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt cleanup -csh -pack=Ds2KKPiPi0Alg -version=Ds2KKPiPi0-master -path=/afs/ihep.ac.cn/users/h/hancj/besfs/boss-7.0.3/workarea  $* >${cmtDs2KKPiPi0Algtempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/mgr/cmt cleanup -csh -pack=Ds2KKPiPi0Alg -version=Ds2KKPiPi0-master -path=/afs/ihep.ac.cn/users/h/hancj/besfs/boss-7.0.3/workarea  $* >${cmtDs2KKPiPi0Algtempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtDs2KKPiPi0Algtempfile}
  unset cmtDs2KKPiPi0Algtempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtDs2KKPiPi0Algtempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtDs2KKPiPi0Algtempfile}
unset cmtDs2KKPiPi0Algtempfile
exit $cmtcleanupstatus

