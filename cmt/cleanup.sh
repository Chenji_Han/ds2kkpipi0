# echo "cleanup Ds2KKPiPi0Alg Ds2KKPiPi0-master in /afs/ihep.ac.cn/users/h/hancj/besfs/boss-7.0.3/workarea"

if test "${CMTROOT}" = ""; then
  CMTROOT=/afs/ihep.ac.cn/bes3/offline/ExternalLib/SLC6/contrib/CMT/v1r25; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtDs2KKPiPi0Algtempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then cmtDs2KKPiPi0Algtempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt cleanup -sh -pack=Ds2KKPiPi0Alg -version=Ds2KKPiPi0-master -path=/afs/ihep.ac.cn/users/h/hancj/besfs/boss-7.0.3/workarea  $* >${cmtDs2KKPiPi0Algtempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/mgr/cmt cleanup -sh -pack=Ds2KKPiPi0Alg -version=Ds2KKPiPi0-master -path=/afs/ihep.ac.cn/users/h/hancj/besfs/boss-7.0.3/workarea  $* >${cmtDs2KKPiPi0Algtempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtDs2KKPiPi0Algtempfile}
  unset cmtDs2KKPiPi0Algtempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtDs2KKPiPi0Algtempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtDs2KKPiPi0Algtempfile}
unset cmtDs2KKPiPi0Algtempfile
return $cmtcleanupstatus

