#-- start of make_header -----------------

#====================================
#  Library Ds2KKPiPi0Alg
#
#   Generated Sun Jul 19 20:18:25 2020  by hancj
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_Ds2KKPiPi0Alg_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_Ds2KKPiPi0Alg_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_Ds2KKPiPi0Alg

Ds2KKPiPi0Alg_tag = $(tag)

#cmt_local_tagfile_Ds2KKPiPi0Alg = $(Ds2KKPiPi0Alg_tag)_Ds2KKPiPi0Alg.make
cmt_local_tagfile_Ds2KKPiPi0Alg = $(bin)$(Ds2KKPiPi0Alg_tag)_Ds2KKPiPi0Alg.make

else

tags      = $(tag),$(CMTEXTRATAGS)

Ds2KKPiPi0Alg_tag = $(tag)

#cmt_local_tagfile_Ds2KKPiPi0Alg = $(Ds2KKPiPi0Alg_tag).make
cmt_local_tagfile_Ds2KKPiPi0Alg = $(bin)$(Ds2KKPiPi0Alg_tag).make

endif

include $(cmt_local_tagfile_Ds2KKPiPi0Alg)
#-include $(cmt_local_tagfile_Ds2KKPiPi0Alg)

ifdef cmt_Ds2KKPiPi0Alg_has_target_tag

cmt_final_setup_Ds2KKPiPi0Alg = $(bin)setup_Ds2KKPiPi0Alg.make
cmt_dependencies_in_Ds2KKPiPi0Alg = $(bin)dependencies_Ds2KKPiPi0Alg.in
#cmt_final_setup_Ds2KKPiPi0Alg = $(bin)Ds2KKPiPi0Alg_Ds2KKPiPi0Algsetup.make
cmt_local_Ds2KKPiPi0Alg_makefile = $(bin)Ds2KKPiPi0Alg.make

else

cmt_final_setup_Ds2KKPiPi0Alg = $(bin)setup.make
cmt_dependencies_in_Ds2KKPiPi0Alg = $(bin)dependencies.in
#cmt_final_setup_Ds2KKPiPi0Alg = $(bin)Ds2KKPiPi0Algsetup.make
cmt_local_Ds2KKPiPi0Alg_makefile = $(bin)Ds2KKPiPi0Alg.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)Ds2KKPiPi0Algsetup.make

#Ds2KKPiPi0Alg :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'Ds2KKPiPi0Alg'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = Ds2KKPiPi0Alg/
#Ds2KKPiPi0Alg::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

Ds2KKPiPi0Alglibname   = $(bin)$(library_prefix)Ds2KKPiPi0Alg$(library_suffix)
Ds2KKPiPi0Alglib       = $(Ds2KKPiPi0Alglibname).a
Ds2KKPiPi0Algstamp     = $(bin)Ds2KKPiPi0Alg.stamp
Ds2KKPiPi0Algshstamp   = $(bin)Ds2KKPiPi0Alg.shstamp

Ds2KKPiPi0Alg :: dirs  Ds2KKPiPi0AlgLIB
	$(echo) "Ds2KKPiPi0Alg ok"

#-- end of libary_header ----------------

Ds2KKPiPi0AlgLIB :: $(Ds2KKPiPi0Alglib) $(Ds2KKPiPi0Algshstamp)
	@/bin/echo "------> Ds2KKPiPi0Alg : library ok"

$(Ds2KKPiPi0Alglib) :: $(bin)Tracks.o $(bin)Ds2KKPiPi0.o $(bin)IsPi0.o $(bin)MCtruthInfo.o $(bin)Load.o $(bin)Entries.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(Ds2KKPiPi0Alglib) $?
	$(lib_silent) $(ranlib) $(Ds2KKPiPi0Alglib)
	$(lib_silent) cat /dev/null >$(Ds2KKPiPi0Algstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(Ds2KKPiPi0Alglibname).$(shlibsuffix) :: $(Ds2KKPiPi0Alglib) $(Ds2KKPiPi0Algstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" Ds2KKPiPi0Alg $(Ds2KKPiPi0Alg_shlibflags)

$(Ds2KKPiPi0Algshstamp) :: $(Ds2KKPiPi0Alglibname).$(shlibsuffix)
	@if test -f $(Ds2KKPiPi0Alglibname).$(shlibsuffix) ; then cat /dev/null >$(Ds2KKPiPi0Algshstamp) ; fi

Ds2KKPiPi0Algclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)Tracks.o $(bin)Ds2KKPiPi0.o $(bin)IsPi0.o $(bin)MCtruthInfo.o $(bin)Load.o $(bin)Entries.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
Ds2KKPiPi0Alginstallname = $(library_prefix)Ds2KKPiPi0Alg$(library_suffix).$(shlibsuffix)

Ds2KKPiPi0Alg :: Ds2KKPiPi0Alginstall

install :: Ds2KKPiPi0Alginstall

Ds2KKPiPi0Alginstall :: $(install_dir)/$(Ds2KKPiPi0Alginstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(Ds2KKPiPi0Alginstallname) :: $(bin)$(Ds2KKPiPi0Alginstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(Ds2KKPiPi0Alginstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(Ds2KKPiPi0Alginstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(Ds2KKPiPi0Alginstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(Ds2KKPiPi0Alginstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(Ds2KKPiPi0Alginstallname) $(install_dir)/$(Ds2KKPiPi0Alginstallname); \
	      echo `pwd`/$(Ds2KKPiPi0Alginstallname) >$(install_dir)/$(Ds2KKPiPi0Alginstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(Ds2KKPiPi0Alginstallname), no installation directory specified"; \
	  fi; \
	fi

Ds2KKPiPi0Algclean :: Ds2KKPiPi0Alguninstall

uninstall :: Ds2KKPiPi0Alguninstall

Ds2KKPiPi0Alguninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(Ds2KKPiPi0Alginstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(Ds2KKPiPi0Alginstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(Ds2KKPiPi0Alginstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(Ds2KKPiPi0Alginstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),Ds2KKPiPi0Algclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Tracks.d

$(bin)$(binobj)Tracks.d :

$(bin)$(binobj)Tracks.o : $(cmt_final_setup_Ds2KKPiPi0Alg)

$(bin)$(binobj)Tracks.o : $(src)Tracks.cxx
	$(cpp_echo) $(src)Tracks.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(Ds2KKPiPi0Alg_pp_cppflags) $(lib_Ds2KKPiPi0Alg_pp_cppflags) $(Tracks_pp_cppflags) $(use_cppflags) $(Ds2KKPiPi0Alg_cppflags) $(lib_Ds2KKPiPi0Alg_cppflags) $(Tracks_cppflags) $(Tracks_cxx_cppflags)  $(src)Tracks.cxx
endif
endif

else
$(bin)Ds2KKPiPi0Alg_dependencies.make : $(Tracks_cxx_dependencies)

$(bin)Ds2KKPiPi0Alg_dependencies.make : $(src)Tracks.cxx

$(bin)$(binobj)Tracks.o : $(Tracks_cxx_dependencies)
	$(cpp_echo) $(src)Tracks.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Ds2KKPiPi0Alg_pp_cppflags) $(lib_Ds2KKPiPi0Alg_pp_cppflags) $(Tracks_pp_cppflags) $(use_cppflags) $(Ds2KKPiPi0Alg_cppflags) $(lib_Ds2KKPiPi0Alg_cppflags) $(Tracks_cppflags) $(Tracks_cxx_cppflags)  $(src)Tracks.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),Ds2KKPiPi0Algclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Ds2KKPiPi0.d

$(bin)$(binobj)Ds2KKPiPi0.d :

$(bin)$(binobj)Ds2KKPiPi0.o : $(cmt_final_setup_Ds2KKPiPi0Alg)

$(bin)$(binobj)Ds2KKPiPi0.o : $(src)Ds2KKPiPi0.cxx
	$(cpp_echo) $(src)Ds2KKPiPi0.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(Ds2KKPiPi0Alg_pp_cppflags) $(lib_Ds2KKPiPi0Alg_pp_cppflags) $(Ds2KKPiPi0_pp_cppflags) $(use_cppflags) $(Ds2KKPiPi0Alg_cppflags) $(lib_Ds2KKPiPi0Alg_cppflags) $(Ds2KKPiPi0_cppflags) $(Ds2KKPiPi0_cxx_cppflags)  $(src)Ds2KKPiPi0.cxx
endif
endif

else
$(bin)Ds2KKPiPi0Alg_dependencies.make : $(Ds2KKPiPi0_cxx_dependencies)

$(bin)Ds2KKPiPi0Alg_dependencies.make : $(src)Ds2KKPiPi0.cxx

$(bin)$(binobj)Ds2KKPiPi0.o : $(Ds2KKPiPi0_cxx_dependencies)
	$(cpp_echo) $(src)Ds2KKPiPi0.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Ds2KKPiPi0Alg_pp_cppflags) $(lib_Ds2KKPiPi0Alg_pp_cppflags) $(Ds2KKPiPi0_pp_cppflags) $(use_cppflags) $(Ds2KKPiPi0Alg_cppflags) $(lib_Ds2KKPiPi0Alg_cppflags) $(Ds2KKPiPi0_cppflags) $(Ds2KKPiPi0_cxx_cppflags)  $(src)Ds2KKPiPi0.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),Ds2KKPiPi0Algclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)IsPi0.d

$(bin)$(binobj)IsPi0.d :

$(bin)$(binobj)IsPi0.o : $(cmt_final_setup_Ds2KKPiPi0Alg)

$(bin)$(binobj)IsPi0.o : $(src)IsPi0.cxx
	$(cpp_echo) $(src)IsPi0.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(Ds2KKPiPi0Alg_pp_cppflags) $(lib_Ds2KKPiPi0Alg_pp_cppflags) $(IsPi0_pp_cppflags) $(use_cppflags) $(Ds2KKPiPi0Alg_cppflags) $(lib_Ds2KKPiPi0Alg_cppflags) $(IsPi0_cppflags) $(IsPi0_cxx_cppflags)  $(src)IsPi0.cxx
endif
endif

else
$(bin)Ds2KKPiPi0Alg_dependencies.make : $(IsPi0_cxx_dependencies)

$(bin)Ds2KKPiPi0Alg_dependencies.make : $(src)IsPi0.cxx

$(bin)$(binobj)IsPi0.o : $(IsPi0_cxx_dependencies)
	$(cpp_echo) $(src)IsPi0.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Ds2KKPiPi0Alg_pp_cppflags) $(lib_Ds2KKPiPi0Alg_pp_cppflags) $(IsPi0_pp_cppflags) $(use_cppflags) $(Ds2KKPiPi0Alg_cppflags) $(lib_Ds2KKPiPi0Alg_cppflags) $(IsPi0_cppflags) $(IsPi0_cxx_cppflags)  $(src)IsPi0.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),Ds2KKPiPi0Algclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)MCtruthInfo.d

$(bin)$(binobj)MCtruthInfo.d :

$(bin)$(binobj)MCtruthInfo.o : $(cmt_final_setup_Ds2KKPiPi0Alg)

$(bin)$(binobj)MCtruthInfo.o : $(src)MCtruthInfo.cxx
	$(cpp_echo) $(src)MCtruthInfo.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(Ds2KKPiPi0Alg_pp_cppflags) $(lib_Ds2KKPiPi0Alg_pp_cppflags) $(MCtruthInfo_pp_cppflags) $(use_cppflags) $(Ds2KKPiPi0Alg_cppflags) $(lib_Ds2KKPiPi0Alg_cppflags) $(MCtruthInfo_cppflags) $(MCtruthInfo_cxx_cppflags)  $(src)MCtruthInfo.cxx
endif
endif

else
$(bin)Ds2KKPiPi0Alg_dependencies.make : $(MCtruthInfo_cxx_dependencies)

$(bin)Ds2KKPiPi0Alg_dependencies.make : $(src)MCtruthInfo.cxx

$(bin)$(binobj)MCtruthInfo.o : $(MCtruthInfo_cxx_dependencies)
	$(cpp_echo) $(src)MCtruthInfo.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Ds2KKPiPi0Alg_pp_cppflags) $(lib_Ds2KKPiPi0Alg_pp_cppflags) $(MCtruthInfo_pp_cppflags) $(use_cppflags) $(Ds2KKPiPi0Alg_cppflags) $(lib_Ds2KKPiPi0Alg_cppflags) $(MCtruthInfo_cppflags) $(MCtruthInfo_cxx_cppflags)  $(src)MCtruthInfo.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),Ds2KKPiPi0Algclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Load.d

$(bin)$(binobj)Load.d :

$(bin)$(binobj)Load.o : $(cmt_final_setup_Ds2KKPiPi0Alg)

$(bin)$(binobj)Load.o : $(src)components/Load.cxx
	$(cpp_echo) $(src)components/Load.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(Ds2KKPiPi0Alg_pp_cppflags) $(lib_Ds2KKPiPi0Alg_pp_cppflags) $(Load_pp_cppflags) $(use_cppflags) $(Ds2KKPiPi0Alg_cppflags) $(lib_Ds2KKPiPi0Alg_cppflags) $(Load_cppflags) $(Load_cxx_cppflags) -I../src/components $(src)components/Load.cxx
endif
endif

else
$(bin)Ds2KKPiPi0Alg_dependencies.make : $(Load_cxx_dependencies)

$(bin)Ds2KKPiPi0Alg_dependencies.make : $(src)components/Load.cxx

$(bin)$(binobj)Load.o : $(Load_cxx_dependencies)
	$(cpp_echo) $(src)components/Load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Ds2KKPiPi0Alg_pp_cppflags) $(lib_Ds2KKPiPi0Alg_pp_cppflags) $(Load_pp_cppflags) $(use_cppflags) $(Ds2KKPiPi0Alg_cppflags) $(lib_Ds2KKPiPi0Alg_cppflags) $(Load_cppflags) $(Load_cxx_cppflags) -I../src/components $(src)components/Load.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),Ds2KKPiPi0Algclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Entries.d

$(bin)$(binobj)Entries.d :

$(bin)$(binobj)Entries.o : $(cmt_final_setup_Ds2KKPiPi0Alg)

$(bin)$(binobj)Entries.o : $(src)components/Entries.cxx
	$(cpp_echo) $(src)components/Entries.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(Ds2KKPiPi0Alg_pp_cppflags) $(lib_Ds2KKPiPi0Alg_pp_cppflags) $(Entries_pp_cppflags) $(use_cppflags) $(Ds2KKPiPi0Alg_cppflags) $(lib_Ds2KKPiPi0Alg_cppflags) $(Entries_cppflags) $(Entries_cxx_cppflags) -I../src/components $(src)components/Entries.cxx
endif
endif

else
$(bin)Ds2KKPiPi0Alg_dependencies.make : $(Entries_cxx_dependencies)

$(bin)Ds2KKPiPi0Alg_dependencies.make : $(src)components/Entries.cxx

$(bin)$(binobj)Entries.o : $(Entries_cxx_dependencies)
	$(cpp_echo) $(src)components/Entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Ds2KKPiPi0Alg_pp_cppflags) $(lib_Ds2KKPiPi0Alg_pp_cppflags) $(Entries_pp_cppflags) $(use_cppflags) $(Ds2KKPiPi0Alg_cppflags) $(lib_Ds2KKPiPi0Alg_cppflags) $(Entries_cppflags) $(Entries_cxx_cppflags) -I../src/components $(src)components/Entries.cxx

endif

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: Ds2KKPiPi0Algclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(Ds2KKPiPi0Alg.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

Ds2KKPiPi0Algclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library Ds2KKPiPi0Alg
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)Ds2KKPiPi0Alg$(library_suffix).a $(library_prefix)Ds2KKPiPi0Alg$(library_suffix).s? Ds2KKPiPi0Alg.stamp Ds2KKPiPi0Alg.shstamp
#-- end of cleanup_library ---------------
