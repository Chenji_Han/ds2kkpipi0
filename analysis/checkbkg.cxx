/*************************************************************************
    > File Name: DrawHist.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Sun 29 Mar 2020 03:29:22 PM CST
 ************************************************************************/

#include"bes3plotstyle.C"
#include"./DrawHist.cxx"

void checkbkg() {

    //TFile inputFile("./sample/DsMCSTV4.root");
    TFile inputFile("/afs/ihep.ac.cn/users/h/hancj/scratchfs/test.root");
    TTree* inputTree = (TTree*) inputFile.Get("output");


    TH1D H00("H00","",100,1.9,2.03);
    //inputTree -> Project("H00","Ds_m","phiVeto2==0&&(!noK)&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.095<DsGamma_m&&DsGamma_m<2.125)||(2.1<DsRecoil_m&&DsRecoil_m<2.125))");
    //inputTree -> Project("H00","Ds_m","phiVeto2==0&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.095<DsGamma_m&&DsGamma_m<2.125)||(2.1<DsRecoil_m&&DsRecoil_m<2.125))");
    inputTree -> Project("H00","Ds_m","phiVeto2==0&&(mode1!=0||mode2!=0)&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.095<DsGamma_m&&DsGamma_m<2.125)||(2.1<DsRecoil_m&&DsRecoil_m<2.125))");
    //inputTree -> Project("H00","Ds_m","");
    draw(&H00, "M(K^{+}K^{+}#pi^{-}#pi^{0})", "checkbkg01");

    //cout<<"events: "<<inputTree -> GetEntries("(mode1!=0||mode2!=0)&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30")<<endl;;
    //cout<<"events: "<<inputTree -> GetEntries("phiVeto2==0&&(mode1!=0||mode2!=0)&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30")<<endl;;
    //cout<<endl<<inputTree->GetEntries("(Ds_m>1.9&&Ds_m<2.03)&&phiVeto2==0&&(mode1!=0||mode2!=0)&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.095<DsGamma_m&&DsGamma_m<2.125)||(2.1<DsRecoil_m&&DsRecoil_m<2.125))")<<endl;

}


