/*************************************************************************
    > File Name: drawHist.cxx
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Tue 02 Jul 2019 08:42:43 PM CST
 ************************************************************************/
#include"bes3plotstyle.C"
#include"TTree.h"
#include"TFile.h"
#include"TH1.h"
#include"string.h"
#include<iostream>
#include"TCanvas.h"
#include"TStyle.h"
#include"LibChenji.cxx"

using namespace std;

void setLatex(TLatex* latex,double size){
	
	latex->SetNDC();
    latex->SetTextFont(12);
    latex->SetTextSize(size);
    latex->SetTextAlign(33);
}

void draw_sub(TH1D* Hist,string title,string XTitle,string saveName){

	SetStyle();
	gStyle -> SetOptStat(0);
	TCanvas C;

	double YMax = Hist -> GetMaximum();

	Hist -> GetYaxis() -> SetRangeUser(0,1.2*YMax);
	Hist -> GetXaxis() -> SetTitle(XTitle.c_str());
	Hist -> GetYaxis() -> SetTitle("events");
	Hist -> SetTitle(title.c_str());
	
	Hist -> Draw("hist");

	TLatex * prelim = new TLatex(0.85, 0.83,"#color[4]{#font[12]{Preliminary}}");
	TLatex * comment1 = new TLatex(0.85,0.78,"K^{2}_{vf}(#Lambda)<20");
	TLatex * comment2 = new TLatex(0.85,0.73,"K^{2}_{svf}(#Xi)<20");
	//TLatex * comment3 = new TLatex(0.85,0.68,"decay length > 2*decay error");
	TLatex * comment4 = new TLatex(0.85,0.68,"#Lambda mass windows");
//	TLatex * comment5 = new TLatex(0.85,0.63,"K^{2}_{vf}(#Xi)<500");
	//TLatex * comment6 = new TLatex(0.85,0.58,"#Xi decay length>2*decay error");
	TLatex * comment7 = new TLatex(0.85,0.63,"recoil mass windows");

    setLatex(prelim,0.042);
	setLatex(comment1,0.037);
	setLatex(comment2,0.037);
	setLatex(comment4,0.037);
//	setLatex(comment5,0.037);
	setLatex(comment7,0.037);

	prelim -> Draw("same");
	comment1 -> Draw("same");
	comment2 -> Draw("same");
	comment4 -> Draw("same");
//	comment5 -> Draw("same");
	comment7 -> Draw("same");

	C.SaveAs(saveName.c_str());

}

void drawHist(){

	string inputTreeName;
	vector<string> inputName;
	string title;
	double dE_min,dE_max,dE_bin;
	double Mbc_min,Mbc_max,Mbc_bin;
	double MXi_min,MXi_max,MXi_bin;
	string line;
    std::ifstream inputTxt("inputs.txt");
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
							    
		std::istringstream iss(line);
        string a,b,c,d;
        iss>>a>>b>>c>>d;

		if(a=="draw_inputName"){
			inputName.push_back(b);
		}else if(a=="draw_MXi"){
			MXi_bin = string2double(b);
			MXi_min = string2double(c);
			MXi_max = string2double(d);
		}else if(a=="draw_inputTreeName"){
			inputTreeName = b;
		}
	}

	TChain* inputTree = new TChain(inputTreeName.c_str());
	for(int i=0;i<inputName.size();i++){
		cout<<"adding "<<inputName[i]<<endl;
		inputTree -> Add(inputName[i].c_str());
	}

	double mXi, mXiRecoil;
	if(inputTreeName=="Xi"){
		inputTree -> SetBranchAddress("mXiRecoil",&mXiRecoil);
		inputTree -> SetBranchAddress("mXi",&mXi);
	}else if(inputTreeName=="Xibar"){
		inputTree -> SetBranchAddress("mXibarRecoil",&mXiRecoil);
		inputTree -> SetBranchAddress("mXibar",&mXi);
	}

	int XipassNum = 0;
	int XiRecoilpassNum = 0;

	TH1D* HmXi = new TH1D("mXi","",MXi_bin,MXi_min,MXi_max);
	TH1D* HmXiRecoil = new TH1D("mXibar","",MXi_bin,MXi_min,MXi_max);

	for(int ievt=0;ievt<inputTree->GetEntries();ievt++){
		inputTree -> GetEntry(ievt);
		if(ievt%1000==0) cout<<"---processing..."<<ievt<<endl;
		if(MXi_min<mXiRecoil&&mXiRecoil<MXi_max){ 
			XipassNum++;
			HmXi -> Fill(mXi);
			HmXiRecoil -> Fill(mXiRecoil);
		}
	}

	draw_sub(HmXi,"mass distribution of reconstructed #Xi","mass of #Xi /GeV","mXi.png");
	draw_sub(HmXiRecoil,"recoil mass distribution","recoil mass/GeV","mXiRecoil.png");


	cout<<"total reconstructed Xi "<<XipassNum<<endl;

	return;

}
