#include"bes3plotstyle.C"
using namespace RooFit;
using namespace std;

void Fit_sub( double& _mean,double& _sigma, double& _a1,double& _a2,double& _nsig,double& _nbkg, RooKeysPdf* mcShape,string inputName)
{

    TFile *inputFile = new TFile(inputName.c_str());
    TTree *Tree = (TTree*)inputFile->Get("Xi");

    int evtNums = Tree -> GetEntries();
    RooRealVar mXi("mXip4","",1.29,1.35);
    RooDataSet data = RooDataSet("data","data after cut",Tree,mXi);

    RooRealVar gmean("mean","",_mean,-0.01,0.01);
    RooRealVar gsigma("sigma","",_sigma,0,0.1);
    RooGaussian covg("covg","the gauss to cov mc shape",mXi,gmean,gsigma);
	RooFFTConvPdf sigPdf("sigPdf","",mXi,*mcShape,covg);

    RooRealVar a1("a1","",_a1,-2,2);
    RooRealVar a2("a2","",_a2,-2,2);
    RooPolynomial bkgPdf("bkgpdf","Argus PDF",mXi,RooArgList(a1,a2));

    RooRealVar nsig("nsig","#signal Events",_nsig,0,evtNums);
    RooRealVar nbkg("nbkg","#background Events",_nbkg,0,evtNums);
    
	RooAddPdf model("model","sig+bkg",RooArgList(sigPdf,bkgPdf),RooArgList(nsig,nbkg));

	RooFitResult *result = model.fitTo(data,Save(kTRUE),Extended());

    _mean = gmean.getValV();
    _sigma = gsigma.getValV();
    _a1 = a1.getValV();
    _a2 = a2.getValV();
    _nsig = nsig.getValV();
    _nbkg = nbkg.getValV();

}



void Fit_recursive(string inputName,string inputShapeName,string flag){

    cout<<"loading the shape p.d.f.  "<<inputShapeName<<endl;
	TFile * inputShape = new TFile(inputShapeName.c_str());
	RooKeysPdf* mcShape = inputShape->Get("sigPDF");	
		
    double sigma = 0.001; double mean = -0.0001; double a1 = -0.1; double a2 = -0.5; double nsig = 160000; double nbkg = 300;
    
    Fit_sub(mean,sigma,a1,a2,nsig,nbkg, mcShape,inputName); 
    
    int num = 0;
    for(num = 0; num < 100; num++ )
    {
        double sigma_tem = sigma; double mean_tem = mean;
        double a1_tem = a1; double a2_tem = a2; 
        double nsig_tem = nsig; double nbkg_tem = nbkg;

        cout<<mean<<endl;
        Fit_sub( mean,sigma,a1,a2,nsig,nbkg,mcShape,inputName); 
        cout<<mean<<endl;
        if( TMath::Abs(mean_tem - mean) < 0.000001 )
        {
            cout<<"number of iterations: "<<num<<endl;
            break; 
        }
    }

    ofstream outputTxtFile("iteration.txt",ios::app);
    if( !outputTxtFile ){
        cout<<" Error in opening output.txt " <<endl;
        return;
    }
    outputTxtFile<<flag<<"  "<<num<<"  "<<1.32132+mean<<"  "<<sigma<<"  "<<a1<<"  "<<a2<<"  "<<nsig<<"  "<<nbkg<<endl;
    outputTxtFile.close();

}








