/*************************************************************************
    > File Name: checklbd.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Wed 01 Apr 2020 04:27:49 PM CST
 ************************************************************************/

#include"bes3plotstyle.C"
#include"LibChenji.cxx"

void compare_sub(TH1D* Hreco,TH1D* Htruth,string titleName,int binNum,double axisMin,double axisMax,string storeName){

    // SetStyle();

    TCanvas* c=new TCanvas("c","",800,600);
    SetStyle();

    TPad *pad1 = new TPad("pad1", "pad1", 0, 0.35, 1, 1.0);
    pad1->SetBottomMargin(0); // Upper and lower plot are joined
    pad1->SetGridx();         // Vertical grid
    pad1->Draw();             // Draw the upper pad: pad1
    
    pad1->cd();               // pad1 becomes the current pad
    //SetStyle();
    Hreco->SetStats(0);          // No statistics on upper plot
    Htruth->SetStats(0);          // No statistics on upper plot
    // gPad->SetOptTitle(0);
    //gPad->SetTitle("");
    Htruth->SetMarkerSize(0);
    Hreco->SetMarkerSize(0);
    
    TLegend* legend = new TLegend(0.60,0.67,0.90,0.87);
    legend -> AddEntry(Hreco,"right #Lambda combination");
    legend -> AddEntry(Htruth,"wrong #Lambda combination");
    
    legend->SetFillColor(0);
  
    Htruth->Scale( 1.0 / Htruth->Integral() );
    Hreco->Scale( 1.0 / Hreco->Integral() );
  
    FormatAxis(Hreco->GetYaxis());
    FormatAxis(Hreco->GetXaxis());
    Hreco->GetYaxis()->SetTitleOffset(0.8);
 
    Hreco->GetYaxis()->SetTitle("Events");
   	
    Hreco->GetXaxis()->SetTitle("");
   	// Hreco->GetXaxis()->SetTitleOffset();
   	Hreco->GetXaxis()->SetTitleSize(0.08);
    Hreco->SetMaximum(Hreco->GetMaximum()*1.2);
    Hreco->GetYaxis()->SetRangeUser(0.001, Hreco->GetMaximum()*1.3);
    //hsig->SetMinimum(1);
    ////gPad->SetLogy();
    TGaxis* xaxis = (TGaxis*)Hreco->GetYaxis();
    xaxis->SetMaxDigits(3);

    Hreco->SetLineColor(kBlue+1);
   	Hreco->SetLineWidth(2);

   	// hsig2 settings
   	Htruth->SetLineColor(kRed);
   	Htruth->SetLineWidth(2);
 //   Htruth->Scale(Hreco->Integral()/Htruth->Integral());
    
    Hreco->Draw("HIST");
    Htruth->Draw("same HIST");
    legend -> Draw("same");
    
    pad1->Update();

	// lower plot will be in pad
   	c->cd();          // Go back to the main canvas before defining pad2
    SetStyle();
   	TPad *pad2 = new TPad("pad2", "pad2", 0, 0.0, 1, 0.35);
   	pad2->SetTopMargin(0.001);
   	pad2->SetBottomMargin(0.4);
   	pad2->SetGridx(); // vertical grid
   	pad2->Draw();
   	pad2->cd();       // pad2 becomes the current pad

	// Define the ratio plot
    Double_t binerr;
    TH1F* h3 = new TH1F("h3","h3",binNum,axisMin,axisMax);
    TH1F* h4 = new TH1F("h4","h4",binNum,axisMin,axisMax);
    h3->Add(Hreco,Htruth,1,-1);
    for (int i=1; i<=binNum; i++){
        binerr = Hreco->GetBinError(i);
        if (binerr!=0){
            h4->SetBinContent(i, binerr);
        }
    }
    h3->Divide(h3,h4);
    for (int j=1;j<binNum+1;j++){
        h3->SetBinError(j, 1.0);
    }
   	//TH1F *h3 = (TH1F*)hsig->Clone("h3");
   	h3->SetLineColor(kBlack);
   	h3->SetMinimum(-9.9);  // Define Y ..
   	h3->SetMaximum(9.09); // .. range
   	

    h3->SetMarkerStyle(21);

   	// hsig settings
   	// Ratio plot (h3) settings
   	h3->GetXaxis()->SetTitle(titleName.c_str()); 


   	// Y axis ratio plot settings
   	h3->GetYaxis()->SetTitle("#chi");
    h3->SetMarkerSize(0.5);
    FormatAxis(h3->GetYaxis());
    FormatAxis(h3->GetXaxis());
   	h3->GetYaxis()->SetTitleSize(0.13);
   	h3->GetYaxis()->SetLabelSize(0.13); 
   	h3->GetXaxis()->SetLabelSize(0.13); 
    h3->GetXaxis()->SetTitleSize(0.15);
   	h3->GetYaxis()->SetTitleOffset(0.3);
   	h3->GetXaxis()->SetTitleOffset(1.24);
   
   	h3->Draw("E1");       // Draw the ratio plot

    //hsig->Draw("E");
    //hsig2->Draw("same:E");
    //TLegend *leg = new TLegend(0.58,0.7,0.83,0.8 );
    //leg->AddEntry(hsig,"Data");
    //Format(leg);
    //leg->Draw();
    // if you want draw an arrow , add the following statements
    //TArrow *arr = new TArrow(0,1000,0,0,0.01,">");
    //Format(arr);
    //arr->Draw(); 
    
    c->SaveAs((string("./plots/")+storeName+string(".png")).c_str());
    c->SaveAs((string("./plots/")+storeName+string(".eps")).c_str());

    delete h3;
    delete h4;
    delete c;

    return;
}


void draw_com(TH1D* H1, string name1, TH1D* H2, string name2,string storeName){

    SetStyle();
	gStyle -> SetOptStat(0);

    Format(H1);
    Format(H2);
    TCanvas C("C","",800,600);
 
    TLegend legend(0.50,0.74,0.90,0.94);
    legend.AddEntry(H1,name1.c_str());
    legend.AddEntry(H2,name2.c_str());
    legend.SetFillColor(0);
	
    H1->SetLineColor(kBlue+1);
   	H1->SetLineWidth(2);

   	H2->SetLineColor(kRed);
   	H2->SetLineWidth(2);
    
    H1 -> GetXaxis() -> SetTitle("M(#Xi^{-})");
    H2 -> GetXaxis() -> SetTitle("M(#Xi^{-})");

    //H1 -> GetXaxis() -> SetTitle("");
    //H2 -> GetXaxis() -> SetTitle("");
    H1->GetYaxis()->SetRangeUser(0,H1->GetMaximum()*1.25);
    H1->GetYaxis()->SetRangeUser(0,H2->GetMaximum()*1.25);

	H1 -> Draw("hist");
	H2 -> Draw("hist same");
	legend.Draw("same");

    string plotName1 = string("plots/") + storeName + string(".png");
    string plotName2 = string("plots/") + storeName + string(".eps");
	C.SaveAs(plotName1.c_str());
	C.SaveAs(plotName2.c_str());


}



void checklbd() {

    TFile inputFile("sample/signalMC5C.root");
    TTree* inputTree = (TTree*) inputFile.Get("XiTruth");
 
    double ProtonPPX, ProtonPPY, ProtonPPZ ;
    double PionMXiPX, PionMXiPY, PionMXiPZ ;
    double PionMLbdPX, PionMLbdPY, PionMLbdPZ ;

    inputTree -> SetBranchAddress("ProtonPPX", &ProtonPPX);
    inputTree -> SetBranchAddress("ProtonPPY", &ProtonPPY);
    inputTree -> SetBranchAddress("ProtonPPZ", &ProtonPPZ);
    
    inputTree -> SetBranchAddress("PionMXiPX", &PionMXiPX);
    inputTree -> SetBranchAddress("PionMXiPY", &PionMXiPY);
    inputTree -> SetBranchAddress("PionMXiPZ", &PionMXiPZ);

    inputTree -> SetBranchAddress("PionMLbdPX", &PionMLbdPX);
    inputTree -> SetBranchAddress("PionMLbdPY", &PionMLbdPY);
    inputTree -> SetBranchAddress("PionMLbdPZ", &PionMLbdPZ);

    TH1D HRight("HRight","", 100, 1.1, 1.17); 
    TH1D HWrong("HWrong","", 100, 1.1, 1.17); 

    const double mpion    = 0.13957;
    const double mproton  = 0.938272;

    for(int ievt=0; ievt<inputTree->GetEntries(); ievt++ ){
        inputTree -> GetEntry(ievt);

        double protonE = sqrt( ProtonPPX*ProtonPPX + 
                               ProtonPPY*ProtonPPY +
                               ProtonPPZ*ProtonPPZ +
                               mproton*mproton );

        double pionXiE = sqrt( PionMXiPX*PionMXiPX + 
                               PionMXiPY*PionMXiPY +
                               PionMXiPZ*PionMXiPZ +
                               mpion*mpion );

        double pionLbdE = sqrt( PionMLbdPX*PionMLbdPX + 
                                PionMLbdPY*PionMLbdPY +
                                PionMLbdPZ*PionMLbdPZ +
                                mpion*mpion );

        double m_right = sqrt( (protonE + pionLbdE) * (protonE + pionLbdE) - 
                               (ProtonPPX + PionMLbdPX) * (ProtonPPX + PionMLbdPX) -
                               (ProtonPPY + PionMLbdPY) * (ProtonPPY + PionMLbdPY) -
                               (ProtonPPZ + PionMLbdPZ) * (ProtonPPZ + PionMLbdPZ) );

        double m_wrong = sqrt( (protonE + pionXiE) * (protonE + pionXiE) - 
                               (ProtonPPX + PionMXiPX) * (ProtonPPX + PionMXiPX) -
                               (ProtonPPY + PionMXiPY) * (ProtonPPY + PionMXiPY) -
                               (ProtonPPZ + PionMXiPZ) * (ProtonPPZ + PionMXiPZ) );

        HWrong.Fill(m_wrong);
        HRight.Fill(m_right);

    }

    draw_com(&HRight,"right combination of #Lambda",&HWrong,"wrong combination of #Lambda","checklbd");

//    compare_sub(&HRight, &HWrong, string("mass #Lambda"),100, 1.1, 1.17, string("lbdreco") );

    return;
}





