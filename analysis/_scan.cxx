#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include <map>
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <math.h>
#include "TMath.h"
#include "TAxis.h"
#include "TCut.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TFrame.h"
#include "TNtuple.h"
#include "TPostScript.h"
#include "RooFit.h"
#include "RooRealVar.h"
#include "RooGaussian.h"
#include "RooNovosibirsk.h"
#include "RooCBShape.h"
#include "RooChebychev.h"
#include "RooBreitWigner.h"
#include "RooDataHist.h"
#include "RooDataSet.h"
#include "RooArgusBG.h"
#include "RooArgList.h"
#include "RooAddPdf.h"
#include "RooAbsPdf.h"
#include "RooKeysPdf.h"
#include "RooPlot.h"
#include "RooWorkspace.h"
#include "/afs/ihep.ac.cn/users/l/lincx/useful/boot.h"
#include "./bes3plotstyle.C"

using namespace RooFit;

void _scan()
{
		// **************** Global Settings *****************
		setStyle();
        myStyle->SetTitleXSize(0.06);myStyle->SetTitleXOffset(1.2);
		myStyle->SetTitleYSize(0.05);myStyle->SetTitleYOffset(1.2);
		// set ytitle
		Double_t xmin=1.9, xmax=2.03;  Double_t xbins=100;
		TH1::SetDefaultSumw2(1);
		TString a("Events/"); char b[20];  sprintf(b, "(%5.1f",(xmax-xmin)/xbins  *1000); TString d(" MeV)");
		TString ytitle = a + b + d;
		//define variables
		RooRealVar Ds_m("Ds_m", "", 1.9, 2.03);
		TString BranName("Ds_m");

		// ***************** Cut ******************
	    TCut cut_sum = "phiVeto2==0&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.1<DsGamma_m&&DsGamma_m<2.13)||(2.1<DsRecoil_m&&DsRecoil_m<2.13))";
	    //TCut cut_sum = "phiVeto2==0&&(mode1!=0||mode2!=0)&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.105<DsGamma_m&&DsGamma_m<2.117)||(2.105<DsRecoil_m&&DsRecoil_m<2.117))";
	    //TCut cut_sum = "phiVeto2==0&&(mode1!=0||mode2!=0)&&(-0.02<DeltaE&&DeltaE<0.02)&&Chisq1C<30&&((2.05<DsGamma_m&&DsGamma_m<2.13)||(2.05<DsRecoil_m&&DsRecoil_m<2.13))";
	    //TCut cut_sum = "(mode1!=0||mode2!=0)&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.05<DsGamma_m&&DsGamma_m<2.13)||(2.05<DsRecoil_m&&DsRecoil_m<2.13))";
		// ************ Input ***************
    
        TFile *inputFile = new TFile("~/DsDataSTV5.root");
        //TFile *inputFile = new TFile("~/scratchfs/DsDataSTV5.root");
        TTree *inputTree = (TTree*)inputFile->Get("output");
        TFile tempTree("d.root","recreate");
        TTree* Tree = inputTree->CopyTree(cut_sum);
        int entries = Tree->GetEntries();
        RooDataSet *data = new RooDataSet("data", "data", Tree, RooArgSet(Ds_m));

		// *************************** mBC signal pdf ****************************
		RooRealVar sigma("sigma", "sigma", 0.001, 0, 0.01);
		RooRealVar mean("mean","mean",0, -0.1, 0.1) ;
		RooGaussModel gauss("gauss","gauss", Ds_m, mean, sigma);
		
        //TFile *file_sig_pdf  = TFile::Open("Ds_m_pdf.root");  // umiss distribution pdf
		//RooWorkspace* w = (RooWorkspace*) file_sig_pdf->Get("w");
		//w->Print();  // debug in here!!!
		//Ds_m= w->var("Ds_m");
		////RooAbsPdf *sig = w->pdf("pdf");
		//RooAbsPdf *SignalPdf = w->pdf("pdf");
		//mbc_1.setBins(10000, "fft");
		//RooAbsPdf *SignalPdf=new RooFFTConvPdf("SignalPdf","sig (X) gauss", umiss1, *sig, gauss);
	
	    TFile * _inputShape = new TFile("./Ds_m_pdf.root");
	    //TFile * _inputShape = new TFile("./Ds_m_pdf_notatall.root");
	    RooKeysPdf* SignalPdf = _inputShape->Get("sigPDF");	
        SignalPdf->SetName("SignalPdf"):

		// *************************** umiss bg pdf *******************************
		RooRealVar *c0= new RooRealVar("c0", "coefficient #0", -5,5);
		RooRealVar *c1= new RooRealVar("c1", "coefficient #1", -5,5);
		RooRealVar *c2= new RooRealVar("c2", "coefficient #2", -10,10);
		RooRealVar *c3= new RooRealVar("c3", "coefficient #3", -50,50);
		RooChebychev *BkgPdf = new RooChebychev("BkgPdf","background component",Ds_m, RooArgList(*c0,*c1));

		// ******************************* total pdf *****************************
		RooRealVar nsig("nsig", "#sig events", 0, entries);
		RooRealVar nbkg("nbkg", "#bkg events", 0, entries);
		RooAddPdf sum("sum", "sum", RooArgList(*SignalPdf,*BkgPdf), RooArgList(nsig,nbkg));

		// ************************************* Fit and Draw *******************************************
		sum.fitTo(*data, Extended(kTRUE));
		//sum.fitTo(*data);  

		RooPlot* frame = Ds_m.frame();
        frame->GetYaxis()->SetRangeUser(0, 10);
		data->plotOn(frame, RooFit::Name("data"), Binning(40), LineWidth(3), MarkerStyle(20), MarkerSize(1));
		sum.plotOn(frame, RooFit::Name("fit"), LineColor(kRed), LineWidth(5));
		sum.plotOn(frame, RooFit::Name("sig"), Components(*SignalPdf), LineStyle(2), LineColor(kGreen),LineWidth(4));
		sum.plotOn(frame, RooFit::Name("bkg"), Components(*BkgPdf), LineStyle(2), LineColor(kBlue),LineWidth(4));

		frame->GetXaxis()->SetTitle("M(K^{+}K^{+}#pi^{-}#pi^{0}) (GeV)");
		frame->GetYaxis()->SetTitle("Events/(0.0013 GeV)");
		frame->GetYaxis()->SetTitleOffset(0.9);
		frame->SetMaximum(200);
		TCanvas *can = new TCanvas("can","Ds_m",800,600);
		setPad();
		frame->Draw();
		TLegend *legend = new TLegend(0.661,0.71,0.9,0.91,NULL,"brNDC");
		legend->AddEntry(frame->findObject("data"), "Real data","lp");
		legend->AddEntry(frame->findObject("fit"), "Fit result","lp");
		legend->AddEntry(frame->findObject("sig"), "Signal","lp");
		legend->AddEntry(frame->findObject("bkg"), "Background","lp");
		legend->SetFillColor(0);
		legend->SetBorderSize(0);
		legend->SetLineColor(1);
		legend->SetLineStyle(1);
		legend->SetLineWidth(1);
		legend->SetFillStyle(1001);
		legend->Draw();

		//hist->Draw();
		can->SetLeftMargin(0.18);
		can->SetRightMargin(0.15);
		can->Update();
		can->SaveAs("plots/Dsval.png");
		can->SaveAs("plots/Dsval.eps");

//return;
		// Scan likelihood
		Double_t stepSize=0.1; Int_t nstep=30000;
		Double_t likelihood[30000], signalNo[30000];
		//scanLikelihood(umiss1, SignalPdf, sum, data, stepSize, nstep, nsig, signalNo, likelihood);                                                                     
		ofstream outfile1("likelihood.txt");
		ofstream outfile2("loglikelihood.txt");
		bool offset=false;
		double offsetValue=0.;
		//bool offset=true;
		//double offsetValue=-2366.81.;
		//double offsetValue=-2361.97;
		const int nbins =nstep;
		for (int i=0;i<nbins;i++)
		{
				double nsignal = i*stepSize;
				nsig.setVal(nsignal);
				nsig.setConstant();
				RooFitResult* r = sum->fitTo(*data,Extended(kTRUE),RooFit::Save(kTRUE));
				r->Print("v");
				if(!offset)
				{
						offsetValue = r->minNll();
						//outfile1<<"#"<<"\t" << offsetValue << std::endl;
						offset = true;
				}
				double loglikelihood = r->minNll()-offsetValue;
				likelihood[i] = exp(-loglikelihood);

				Ds_m.setRange("signal",1.9,2.03);
				RooAbsReal *inteSignal=SignalPdf->createIntegral(Ds_m,NormSet(Ds_m),Range("signal"));
				signalNo[i]=inteSignal->getVal()*nsig.getVal();
				//outfile2<<nsignal<<"\t" << loglikelihood << std::endl;
				outfile1<<nsignal<<"  "<< likelihood[i] << std::endl;
				outfile2<< loglikelihood[i] << std::endl;
		}

		for(Int_t i=0; i<nstep; i++) {
				cout<<"i="<<i<<", signalNo "<<signalNo[i]<<" likelihood "<<likelihood[i]<<endl;
		}   
		TCanvas* c_likelihood = new TCanvas("c_likelihood","", 800,600);
        c_likelihood->SetFillColor(0);
		gStyle->SetOptStat(0);
		//gStyle->SetLabelSize(0.04,"xyz");
		gStyle->SetLineWidth(2);
		Double_t xmax=stepSize*nstep;
		double y_up=2.5;
		c_likelihood->DrawFrame(0,0,xmax,y_up,"");
		TGraph* g2 = new TGraph(nstep, signalNo, likelihood);
        TF1 *f_gaus = new TF1("f_gaus","gaus",0.1,12);
        g2->Fit("f_gaus","R");
		g2->SetTitle("");
		g2->GetXaxis()->SetTitle("Yield");
        FormatAxis(g2->GetXaxis());
		g2->GetYaxis()->SetTitle("LH");
        FormatAxis(g2->GetYaxis());
		g2->SetMarkerColor(2); 
		g2->SetLineColor(2); 
		g2->SetLineWidth(1);
		g2->SetMarkerStyle(20);
		g2->SetMarkerSize(1);
		g2->Draw("APL");

		Double_t num=0, prob=0.9000, lsum=0;
		Int_t J=nstep;
		for(Int_t j=0; j<J; j++) 
		{
				lsum+=likelihood[j];
		}
		Double_t persent=lsum*prob;
		Double_t persentemp(0);
		for(Int_t j=0; j<J; j++)
		{
				persentemp+=likelihood[j];
				if(persentemp>persent)
				{ num=signalNo[j];break; }
		}
		cout<<num<<endl;
		TLine* l1 = new TLine(num,0,num,y_up);
		l1->SetLineColor(kBlue);
		l1->Draw("same");

		TH1F* h = new TH1F("h","h",xbins,xmin,xmax);
		h->SetLineColor(0);
		TLegend *legend= new TLegend(0.31,0.75,0.55,0.86);
        legend->SetBorderSize(0);
		legend->SetFillColor(0);
		char leg1[100];
		TString inclusive[2];
		inclusive[0]="D^{+}_{s}#rightarrowK^{+}K^{+}#pi^{-}#pi^{0}";
		sprintf(leg1,"%s",const_cast<char*>(inclusive[0]));
		legend->AddEntry(h,leg1);
        legend->SetFillColor(0);
		legend->Draw();

		c_likelihood->SaveAs("plots/Ds_m_scan.png");
		c_likelihood->SaveAs("plots/Ds_m_scan.eps");

}

/*
void scanLikelihood(RooRealVar& umiss1, RooAbsPdf* SignalPdf, RooAbsPdf* sum, RooAbsData* data, double& stepSize, int& nstep, RooRealVar& nsig, double signalNo[], double likelihood[])
{
		ofstream outfile1("loglikelihood.txt");
		ofstream outfile2("likelihood.txt");
		bool offset=false;
		double offsetValue=0.;
		const int nbins =nstep;
		for (int i=0;i<nbins;i++)
		{
				double nsignal = i*stepSize;
				nsig.setVal(nsignal);
				nsig.setConstant();
				RooFitResult* r = sum->fitTo(*data,Extended(kTRUE),RooFit::Save(kTRUE));
				r->Print("v");
				if(!offset)
				{
						offsetValue = r->minNll();
						outfile1<<"#"<<"\t" << offsetValue << std::endl;
						offset = true;
				}
				double loglikelihood = r->minNll()-offsetValue;
				likelihood[i] = exp(-loglikelihood);

				umiss1.setRange("signal",-0.1,0.1);
				RooAbsReal *inteSignal=SignalPdf->createIntegral(umiss1,NormSet(umiss1),Range("signal"));
				signalNo[i]=inteSignal->getVal()*nsig.getVal();
				outfile1<<signalNo[i]<<"\t" << loglikelihood << std::endl;
				outfile2<<signalNo[i]<<"\t" << likelihood[i] << std::endl;
		}
}
*/
