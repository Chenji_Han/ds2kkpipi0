/*************************************************************************
    > File Name: addNewBranch.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Sat 18 Apr 2020 03:05:12 PM CST
 ************************************************************************/

#include "DrawHist.cxx"

void drawPhi(){

    TFile inputFile("test.root");
    TTree* inputTree = (TTree*) inputFile.Get("output");

    double phi[20][5];

    inputTree -> SetBranchAddress("KpKm",phi);
  
    int mode1;
    int mode2;
    double DeltaE, Chisq1C;
    int phiVeto2;
    double DsGamma_m, DsRecoil_m;
    inputTree -> SetBranchAddress("mode1",&mode1);
    inputTree -> SetBranchAddress("mode2",&mode2);
    inputTree -> SetBranchAddress("DeltaE",&DeltaE);
    inputTree -> SetBranchAddress("Chisq1C",&Chisq1C);
    inputTree -> SetBranchAddress("DsGamma_m",&DsGamma_m);
    inputTree -> SetBranchAddress("DsRecoil_m",&DsRecoil_m);
 
    TH1D H("H","",100,0.9,1.5);

    
    for(int ievt=0; ievt<inputTree->GetEntries(); ievt++ ){
        inputTree -> GetEntry(ievt);
        if(ievt%10000==0) cout<<ievt<<endl;
 
        if(!((mode1!=0||mode2!=0)&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.09<DsGamma_m&&DsGamma_m<2.13)||(2.09<DsRecoil_m&&DsRecoil_m<2.13))) ) continue;
 
        for(int i=0;i<20;i++){
            H.Fill(phi[i][0]);
        }
    }

    draw(&H, "#phi", "phi");

}

