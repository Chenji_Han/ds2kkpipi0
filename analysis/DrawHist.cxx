/*************************************************************************
    > File Name: DrawHist.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Sun 29 Mar 2020 03:29:22 PM CST
 ************************************************************************/

#include"bes3plotstyle.C"
//#include"./compare.cxx"

void draw_com7(TH1D* H1, string name1, 
               TH1D* H2, string name2,
               TH1D* H3, string name3, 
               TH1D* H4, string name4, 
               TH1D* H5, string name5, 
               TH1D* H6, string name6, 
               TH1D* H7, string name7, 
               string storeName){

    SetStyle();
	gStyle -> SetOptStat(0);

    Format(H1);
    Format(H2);
    Format(H3);
    Format(H4);
    Format(H5);
    Format(H6);
    Format(H7);

    TCanvas C("C","",800,600);
 
    TLegend legend(0.65,0.65,0.90,0.87);
    legend.AddEntry(H1,name1.c_str());
    legend.AddEntry(H2,name2.c_str());
    legend.AddEntry(H3,name3.c_str());
    legend.AddEntry(H4,name4.c_str());
    legend.AddEntry(H5,name5.c_str());
    legend.AddEntry(H6,name6.c_str());
    legend.AddEntry(H7,name7.c_str());

    legend.SetFillColor(0);
	
    H1->SetLineColor(kBlack);
   	H1->SetLineWidth(2);

   	H2->SetLineColor(kRed);
   	H2->SetLineWidth(2);

   	H3->SetLineColor(kBlue);
   	H3->SetLineWidth(2);

    H4->SetLineColor(kYellow);
   	H4->SetLineWidth(2);

    H5->SetLineColor(kGray);
   	H5->SetLineWidth(2);

    H6->SetLineColor(kPink);
   	H6->SetLineWidth(2);

    H7->SetLineColor(kOrange);
   	H7->SetLineWidth(2);


    H1 -> GetXaxis() -> SetTitle("");
    H2 -> GetXaxis() -> SetTitle("");
    H3 -> GetXaxis() -> SetTitle("");
    H4 -> GetXaxis() -> SetTitle("");
    H5 -> GetXaxis() -> SetTitle("");
    H6 -> GetXaxis() -> SetTitle("");
    H7 -> GetXaxis() -> SetTitle("");

    H3 -> SetMaximum( H3->GetMaximum() * 1.25 );
   
	H3 -> Draw("hist");
	H1 -> Draw("hist same");
	H2 -> Draw("hist same");
	H4 -> Draw("hist same");
	H5 -> Draw("hist same");
	H6 -> Draw("hist same");
	H7 -> Draw("hist same");

	legend.Draw("same");

    string plotName1 = string("plots/") + storeName + string(".png");
    string plotName2 = string("plots/") + storeName + string(".eps");
	C.SaveAs(plotName1.c_str());
	C.SaveAs(plotName2.c_str());


}


void draw_com3(TH1D* H1, string name1, TH1D* H2, string name2,TH1D* H3, string name3, string storeName){

    SetStyle();
	gStyle -> SetOptStat(0);

    Format(H1);
    Format(H2);
    Format(H3);
    TCanvas C("C","",800,600);
 
    TLegend legend(0.65,0.65,0.90,0.87);
    legend.AddEntry(H1,name1.c_str());
    legend.AddEntry(H2,name2.c_str());
    legend.AddEntry(H3,name3.c_str());
    legend.SetFillColor(0);
	
    H1->SetLineColor(kBlack);
   	H1->SetLineWidth(2);

   	H2->SetLineColor(kRed);
   	H2->SetLineWidth(2);

   	H3->SetLineColor(kBlue);
   	H3->SetLineWidth(2);

    H1 -> GetXaxis() -> SetTitle(storeName.c_str());
    H2 -> GetXaxis() -> SetTitle(storeName.c_str());
    H3 -> GetXaxis() -> SetTitle(storeName.c_str());

    H3 -> SetMaximum( H3->GetMaximum() * 1.25 );
    H3 -> GetYaxis() -> SetRangeUser(0, H3->GetMaximum()*1.5);
    H2 -> GetYaxis() -> SetRangeUser(0, H3->GetMaximum()*1.5);
    H1 -> GetYaxis() -> SetRangeUser(0, H3->GetMaximum()*1.5);

    //H1 -> GetXaxis() -> SetTitle("");

	H3 -> Draw("hist");
	H1 -> Draw("hist same");
	H2 -> Draw("hist same");
	legend.Draw("same");

    string plotName1 = string("plots/") + storeName + string(".png");
    string plotName2 = string("plots/") + storeName + string(".eps");
	C.SaveAs(plotName1.c_str());
	C.SaveAs(plotName2.c_str());


}



void draw_com(TH1D* H1, string name1, TH1D* H2, string name2,string storeName){

    SetStyle();
	gStyle -> SetOptStat(0);

    Format(H1);
    Format(H2);
    TCanvas C("C","",800,600);
 
    TLegend legend(0.50,0.67,0.90,0.87);
    legend.AddEntry(H1,name1.c_str());
    legend.AddEntry(H2,name2.c_str());
    legend.SetFillColor(0);
	
    H1->SetLineColor(kBlue+1);
   	H1->SetLineWidth(2);

   	H2->SetLineColor(kRed);
   	H2->SetLineWidth(2);

    H1 -> GetXaxis() -> SetTitle("M(#Xi^{-}#bar{#Xi}^{+}#pi^{0}");
    H2 -> GetXaxis() -> SetTitle("M(#Xi^{-}#bar{#Xi}^{+}#pi^{0}");

    //H1 -> GetXaxis() -> SetTitle("");
    //H2 -> GetXaxis() -> SetTitle("");

	H1 -> Draw("hist");
	H2 -> Draw("hist same");
	legend.Draw("same");

    string plotName1 = string("plots/") + storeName + string(".png");
    string plotName2 = string("plots/") + storeName + string(".eps");
	C.SaveAs(plotName1.c_str());
	C.SaveAs(plotName2.c_str());


}


void draw(TH1D* H,string titleName,string storeName){

    SetStyle();
	gStyle -> SetOptStat(0);

    Format(H);
    TCanvas C("C","",800,600);
	H -> GetXaxis() -> SetTitle(titleName.c_str());
	H -> Draw("hist");
    string plotName1 = string("plots/") + storeName + string(".png");
    string plotName2 = string("plots/") + storeName + string(".eps");
	C.SaveAs(plotName1.c_str());
	C.SaveAs(plotName2.c_str());


}

void draw2D( TH2D* H, string Xtitle, string Ytitle, string storeName ){

    SetStyle();
	gStyle -> SetOptStat(0);
	TCanvas C1;
	H -> Draw("COLZ");
    FormatAxis(H->GetYaxis());
    FormatAxis(H->GetXaxis());
    H-> GetXaxis() -> SetTitle(Xtitle.c_str());
    H-> GetYaxis() -> SetTitle(Ytitle.c_str());

    string pngName = string("./plots/") + storeName + string(".png");
    string epsName = string("./plots/") + storeName + string(".eps");
	C1.SaveAs(pngName.c_str());
	C1.SaveAs(epsName.c_str());

}

void DrawHist() {

    //TFile inputFile("./sample/DsMCSTV4.root");
    TFile inputFile("/afs/ihep.ac.cn/users/h/hancj/scratchfs/DsDataSTV5.root");
    //cout<<"loading file "<<"/afs/ihep.ac.cn/users/h/hancj/scratchfs/DsMC/STV4/root/DsMCSTV5.root"<<endl;
    //TFile inputFile("/afs/ihep.ac.cn/users/h/hancj/scratchfs/DsDataSTV5.root");
    TTree* inputTree = (TTree*) inputFile.Get("output");
      
   //TFile _inputFile("/afs/ihep.ac.cn/users/h/hancj/scratchfs/001002.root");
   //TTree* _inputTree = (TTree*) _inputFile.Get("output");
   //
   //TH1D H0s("H0s","",50,1.9,2.03);
   //TH1D H0b("H0b","",50,1.9,2.03);
   //TH1D H0a("H0a","",50,1.9,2.03);
   //_inputTree -> Project("H0s","Ds_m","phiVeto2==0&&(mode1!=0||mode2!=0)&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.09<DsGamma_m&&DsGamma_m<2.13)||(2.09<DsRecoil_m&&DsRecoil_m<2.13))");
   //inputTree  -> Project("H0b","Ds_m","phiVeto2==0&&(mode1!=0||mode2!=0)&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.09<DsGamma_m&&DsGamma_m<2.13)||(2.09<DsRecoil_m&&DsRecoil_m<2.13))");
   //_inputTree -> Project("H0s","Ds_m","phiVeto2==0&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.09<DsGamma_m&&DsGamma_m<2.13)||(2.09<DsRecoil_m&&DsRecoil_m<2.13))");
   // inputTree -> Project("H0b","Ds_m","phiVeto2==0&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.09<DsGamma_m&&DsGamma_m<2.13)||(2.09<DsRecoil_m&&DsRecoil_m<2.13))");
   //_inputTree -> Project("H0s","Ds_m","(!noK)&&phiVeto2==0&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.09<DsGamma_m&&DsGamma_m<2.13)||(2.09<DsRecoil_m&&DsRecoil_m<2.13))");
   // inputTree -> Project("H0b","Ds_m","(!noK)&&phiVeto2==0&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.09<DsGamma_m&&DsGamma_m<2.13)||(2.09<DsRecoil_m&&DsRecoil_m<2.13))");
   


    //H0s.Scale(1.0/40.0);
    //H0b.Scale(1.0/40.0);
    //H0a.Add(&H0s,&H0b);
    //
    //draw_com3(&H0a,"signal+background",&H0s,"signal",&H0b,"backgroung","sig_bkg_K");

    //return;

    //TH2D H2D1("H2D1","",100,2.05,2.18,100,2.05,2.18);
    //inputTree -> Project("H2D1","DsRecoil_m:DsGamma_m");
    //draw2D(&H2D1, "DsGamma", "DsRecoil_m", "DsGamma_DsRecoil");

    //TH2D H2D2("H2D2","",100,1.8,2.1,100,-0.05,0.05);
    //inputTree -> Project("H2D2","DeltaE:Ds_m");
    //draw2D(&H2D2, "Ds", "#DeltaE", "DE_Ds");
    
    //TH1D H00("H00","",50,1.9,2.03);
    //_inputTree -> Project("H00","Ds_m","phiVeto2==0&&(mode1!=0||mode2!=0)&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30");
    //H00.Scale(1.0/40.0);
    //draw(&H00, "M(K^{+}K^{+}#pi^{-}#pi^{0})", "Ds_test_mode12");

    //TH1D H001("H001","",50,1.9,2.03);
    //_inputTree -> Project("H001","Ds_m","(!noK)&&phiVeto2==0&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30");
    //H001.Scale(1.0/40.0);
    //draw(&H001, "M(K^{+}K^{+}#pi^{-}#pi^{0})", "Ds_test_oppositeK");


    //TH1D H002("H002","",50,1.9,2.03);
    //_inputTree -> Project("H002","Ds_m","phiVeto2==0&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30");
    //H002.Scale(1.0/40.0);
    //draw(&H002, "M(K^{+}K^{+}#pi^{-}#pi^{0})", "Ds_test_notAtAll");


    //TH1D H0000("H0000","",60,2.05,2.2);
    //inputTree -> Project("H0000","DsGamma_m","");
    //draw(&H0000, "DsGamma", "DsGamma_m");
 
    //TH1D H000("H000","",60,2.05,2.2);
    //inputTree -> Project("H000","DsRecoil_m","");
    //draw(&H000, "recoil M(K^{+}K^{+}#pi^{-}#pi^{0})", "dt_recoil");

    TH1D H00("H00","",100,1.9,2.03);
    inputTree -> Project("H00","Ds_m","((charge==1&&NKm==0)||(charge==-1&&NKp==0))&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.09<DsGamma_m&&DsGamma_m<2.13)||(2.09<DsRecoil_m&&DsRecoil_m<2.13))");
    //inputTree -> Project("H00","Ds_m","phiVeto2==0&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.09<DsGamma_m&&DsGamma_m<2.13)||(2.09<DsRecoil_m&&DsRecoil_m<2.13))");
    //inputTree -> Project("H00","Ds_m","phiVeto2==0&&(mode1!=0||mode2!=0)&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.09<DsGamma_m&&DsGamma_m<2.13)||(2.09<DsRecoil_m&&DsRecoil_m<2.13))");
    draw(&H00, "M(K^{+}K^{+}#pi^{-}#pi^{0})", "Ds_noKm");

return;
    TH1D H04("H04","",100,0.1,0.2);
    inputTree -> Project("H04","Pi0_m","");
    draw(&H04, "M(Pi0)", "pi0_m");

    TH1D H05("H05","",100,0,2);
    inputTree -> Project("H05","K1K2_m","");
    draw(&H05, "M(K1K2)", "k1k2_m");

    TH1D H06("H06","",100,0.5,1.5);
    inputTree -> Project("H06","K1Pi_m","");
    draw(&H06, "M(K1Pi)", "k1pi_m");

    TH1D H07("H07","",100,0.5,1.5);
    inputTree -> Project("H07","K2Pi_m","");
    draw(&H07, "M(K2Pi)", "k2pi_m");

    TH1D H08("H08","",100,0,2);
    inputTree -> Project("H08","K1Pi0_m","");
    draw(&H08, "M(K1Pi0)", "k1pi0_m");

    TH1D H09("H09","",100,0,2);
    inputTree -> Project("H09","K2Pi0_m","");
    draw(&H09, "M(K2Pi0)", "k2pi0_m");

    //TH1D H10("H10","",100,398,420);
    //inputTree -> Project("H10","st_mode","");
    //draw(&H10, "mode", "mode");

    TH1D H11("H11","",100,-2,2);
    inputTree -> Project("H11","charge","");
    draw(&H11, "charge", "charge");

    TH1D H12("H12","",100,0,150);
    inputTree -> Project("H12","Chisq1C","");
    draw(&H12, "#chi^{2} of 2C", "chisq");

    TH1D H13("H13","",100,-0.1,0.1);
    inputTree -> Project("H13","DeltaE","");
    draw(&H13, "#DeltaE", "deltaE");

    TH1D H14("H14","",100,0.1,1.1);
    inputTree -> Project("H14","PiPi0_m","");
    draw(&H14, "M(#pi#pi^{0})", "PiPi0M");

    TH1D H15("H15","",100,0,10);
    inputTree -> Project("H15","ChisqVF","");
    draw(&H15, "#chi^{2} of VF", "chisqVF");

    //TH1D H16("H16","",100,0,250);
    //inputTree -> Project("H16","Chisq2C","");
    //draw(&H16, "#chi^{2} of 3C", "chisq2C");

    TH1D H17("H17","",100,0,5);
    inputTree -> Project("H17","mode1","");
    draw(&H17, "mode1", "mode1");

    TH1D H18("H18","",100,0,5);
    inputTree -> Project("H18","mode2","");
    draw(&H18, "mode2", "mode2");

    TH1D H19("H19","",100,0,5);
    inputTree -> Project("H19","NEp","");
    draw(&H19, "num of e^{+}", "NEp");

    TH1D H20("H20","",100,0,5);
    inputTree -> Project("H20","NEm","");
    draw(&H20, "num of e^{-}", "NEm");

    //cout<<"events: "<<inputTree -> GetEntries("(mode1!=0||mode2!=0)&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30")<<endl;;
    //cout<<"events: "<<inputTree -> GetEntries("phiVeto2==0&&(mode1!=0||mode2!=0)&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30")<<endl;;
    cout<<endl<<inputTree->GetEntries("(Ds_m>1.9&&Ds_m<2.03)&&phiVeto2==0&&(mode1!=0||mode2!=0)&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.095<DsGamma_m&&DsGamma_m<2.125)||(2.1<DsRecoil_m&&DsRecoil_m<2.125))")<<endl;

}


