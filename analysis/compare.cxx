/*************************************************************************
    > File Name: compare.cxx
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Mon 13 Jan 2020 09:38:33 AM CST
 ************************************************************************/

#include"bes3plotstyle.C"
#include"./LibChenji.cxx"
#include"./DrawHist.cxx"

void draw_com4(TH1D* H1, string name1, TH1D* H2, string name2,TH1D* H3, string name3, TH1D* H4, string name4, string titleName, string storeName){

    SetStyle();
	gStyle -> SetOptStat(0);

    Format(H1);
    Format(H2);
    Format(H3);
    Format(H4);

    TCanvas C("C","",800,600);
 
    TLegend legend(0.65,0.65,0.90,0.87);
    legend.AddEntry(H1,name1.c_str());
    legend.AddEntry(H2,name2.c_str());
    legend.AddEntry(H3,name3.c_str());
    legend.AddEntry(H4,name4.c_str());
    legend.SetFillColor(0);
	
    H1->SetLineColor(kBlack);
   	H1->SetLineWidth(2);

   	H2->SetLineColor(kRed);
   	H2->SetLineWidth(2);

   	H3->SetLineColor(kBlue);
   	H3->SetLineWidth(2);

   	H4->SetLineColor(kYellow);
   	H4->SetLineWidth(2);

    H4 -> GetXaxis() -> SetTitle(titleName.c_str());
    H3 -> GetXaxis() -> SetTitle(titleName.c_str());
    H2 -> GetXaxis() -> SetTitle(titleName.c_str());
    H1 -> GetXaxis() -> SetTitle(titleName.c_str());

    H3 -> SetMaximum( H3->GetMaximum() * 1.25 );
    H3 -> GetYaxis() -> SetRangeUser(0, H3->GetMaximum()*1.5);
    H2 -> GetYaxis() -> SetRangeUser(0, H3->GetMaximum()*1.5);
    H1 -> GetYaxis() -> SetRangeUser(0, H3->GetMaximum()*1.5);
    H4 -> GetYaxis() -> SetRangeUser(0, H3->GetMaximum()*1.5);

    //H1 -> GetXaxis() -> SetTitle("");

	H3 -> Draw("hist");
	H1 -> Draw("hist same");
	H2 -> Draw("hist same");
	H4 -> Draw("hist same");
	legend.Draw("same");

    string plotName1 = string("plots/") + storeName + string(".png");
    string plotName2 = string("plots/") + storeName + string(".eps");
	C.SaveAs(plotName1.c_str());
	C.SaveAs(plotName2.c_str());


}


void compare_sub(TH1D* Hreco,string name1,TH1D* Htruth,string name2, string titleName,int binNum,double axisMin,double axisMax,string storeName){

     SetStyle();

    TCanvas* c=new TCanvas("c","",800,600);
    SetStyle();

    TPad *pad1 = new TPad("pad1", "pad1", 0, 0.35, 1, 1.0);
    pad1->SetBottomMargin(0); // Upper and lower plot are joined
    pad1->SetGridx();         // Vertical grid
    pad1->Draw();             // Draw the upper pad: pad1
    
    pad1->cd();               // pad1 becomes the current pad
    //SetStyle();
    Hreco->SetStats(0);          // No statistics on upper plot
    Htruth->SetStats(0);          // No statistics on upper plot
    // gPad->SetOptTitle(0);
    //gPad->SetTitle("");
    Htruth->SetMarkerSize(0);
    Hreco->SetMarkerSize(0);
     
    Htruth->Scale(1.0/Htruth->Integral());
    Hreco->Scale(1.0/Hreco->Integral());

    TLegend* legend = new TLegend(0.60,0.67,0.90,0.87);
    legend -> AddEntry(Hreco,name1.c_str());
    legend -> AddEntry(Htruth,name2.c_str());
   
    legend->SetFillColor(0);
    
    FormatAxis(Hreco->GetYaxis());
    FormatAxis(Hreco->GetXaxis());
    Hreco->GetYaxis()->SetTitleOffset(0.8);
 
    Hreco->GetYaxis()->SetTitle("Events");
   	
    Hreco->GetXaxis()->SetTitle("");
   	// Hreco->GetXaxis()->SetTitleOffset();
   	Hreco->GetXaxis()->SetTitleSize(0.08);
    Hreco->SetMaximum(Hreco->GetMaximum()*1.2);
    Hreco->GetYaxis()->SetRangeUser(0.001, Hreco->GetMaximum()*1.25);
    //hsig->SetMinimum(1);
    ////gPad->SetLogy();
    TGaxis* xaxis = (TGaxis*)Hreco->GetYaxis();
    xaxis->SetMaxDigits(3);

    Hreco->SetLineColor(kBlue+1);
   	Hreco->SetLineWidth(2);

   	// hsig2 settings
   	Htruth->SetLineColor(kRed);
   	Htruth->SetLineWidth(2);
    Htruth->Scale(Hreco->Integral()/Htruth->Integral());
    Htruth->Scale(Hreco->Integral()/Htruth->Integral());

    Hreco->Draw("HIST");
    Htruth->Draw("same HIST");
    legend -> Draw("same");
    
    pad1->Update();

	// lower plot will be in pad
   	c->cd();          // Go back to the main canvas before defining pad2
    SetStyle();
   	TPad *pad2 = new TPad("pad2", "pad2", 0, 0.0, 1, 0.35);
   	pad2->SetTopMargin(0.001);
   	pad2->SetBottomMargin(0.4);
   	pad2->SetGridx(); // vertical grid
   	pad2->Draw();
   	pad2->cd();       // pad2 becomes the current pad

	// Define the ratio plot
    Double_t binerr;
    TH1F* h3 = new TH1F("h3","h3",binNum,axisMin,axisMax);
    TH1F* h4 = new TH1F("h4","h4",binNum,axisMin,axisMax);
    h3->Add(Hreco,Htruth,1,-1);
    for (int i=1; i<=binNum; i++){
        binerr = Hreco->GetBinError(i);
        if (binerr!=0){
            h4->SetBinContent(i, binerr);
        }
    }
    h3->Divide(h3,h4);
    for (int j=1;j<binNum+1;j++){
        h3->SetBinError(j, 1.0);
    }
   	//TH1F *h3 = (TH1F*)hsig->Clone("h3");
   	h3->SetLineColor(kBlack);
   	h3->SetMinimum(-9.9);  // Define Y ..
   	h3->SetMaximum(9.09); // .. range
   	
    h3->SetMarkerStyle(21);

   	// hsig settings
   	// Ratio plot (h3) settings
   	h3->GetXaxis()->SetTitle(titleName.c_str()); 


   	// Y axis ratio plot settings
   	h3->GetYaxis()->SetTitle("#chi");
    h3->SetMarkerSize(0.5);
    FormatAxis(h3->GetYaxis());
    FormatAxis(h3->GetXaxis());
   	h3->GetYaxis()->SetRangeUser(-0.1,0.1);
   	h3->GetYaxis()->SetTitleSize(0.13);
   	h3->GetYaxis()->SetLabelSize(0.13); 
   	h3->GetXaxis()->SetLabelSize(0.13); 
    h3->GetXaxis()->SetTitleSize(0.15);
   	h3->GetYaxis()->SetTitleOffset(0.3);
   	h3->GetXaxis()->SetTitleOffset(1.24);
   
   	h3->Draw("E1");       // Draw the ratio plot

    //hsig->Draw("E");
    //hsig2->Draw("same:E");
    //TLegend *leg = new TLegend(0.58,0.7,0.83,0.8 );
    //leg->AddEntry(hsig,"Data");
    //Format(leg);
    //leg->Draw();
    // if you want draw an arrow , add the following statements
    //TArrow *arr = new TArrow(0,1000,0,0,0.01,">");
    //Format(arr);
    //arr->Draw(); 
    
    c->SaveAs((string("./plots/")+storeName+string(".png")).c_str());
    c->SaveAs((string("./plots/")+storeName+string(".eps")).c_str());

    delete h3;
    delete h4;
    delete c;

    return;
}

void compare(){
        
    SetStyle();
    gStyle->SetPadRightMargin(0.15);

    vector<string> Name1;
    vector<string> Name2;
    vector<int> Bin;
    vector<double> Min;
    vector<double> Max;

    string line;
    std::ifstream inputTxt("inputs.txt");
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
							    
		std::istringstream iss(line);
        string a,b,c,d,e,f;
        iss>>a>>b>>c>>d>>e>>f;

		if(a=="compare"){
            Name1.push_back(b);
            Name2.push_back(c);
            Bin.push_back(string2double(d));
            Min.push_back(string2double(e));
            Max.push_back(string2double(f));
        }
	}

    TFile* inputFile1 = new TFile("/workfs/bes/hancj/DATA/DsIncSTV5.root");
    TTree *inputTree1 = (TTree*) inputFile1->Get("output");
    TFile* inputFile2 = new TFile("~/scratchfs/DsDataSTV5.root");
    TTree *inputTree2 = (TTree*) inputFile2->Get("output");
    TFile* inputFile3 = new TFile("/workfs/bes/hancj/DATA/DsMCSTV5.root");
    TTree *inputTree3 = (TTree*) inputFile1->Get("output");
   
    for(int i=0;i<Name1.size();i++){
        cout<<Name1[i]<<endl;

        TH1D* H_mc_sb = new TH1D("mc_sb","",Bin[i],Min[i],Max[i]); 
        TH1D* H_mc_signal = new TH1D("mc_signal","",Bin[i],Min[i],Max[i]); 
        TH1D* H_data_sb = new TH1D("data_sb","",Bin[i],Min[i],Max[i]); 
        TH1D* H_data_signal = new TH1D("data_signal","",Bin[i],Min[i],Max[i]); 

        inputTree2 -> Project("data_sb",Name1[i].c_str(),"Ds_m<1.94||Ds_m>2.0"); 
        inputTree2 -> Project("data_signal",Name1[i].c_str(),"Ds_m>1.94&&Ds_m<2.0"); 
        inputTree1 -> Project("mc_sb",Name2[i].c_str(),"Ds_m<1.94||Ds_m>2.0"); 
        inputTree3 -> Project("mc_signal",Name2[i].c_str(),"Ds_m>1.94&&Ds_m<2.0"); 

        H_mc_sb -> Scale( 1 / H_mc_sb -> Integral() );
        H_mc_signal -> Scale( 1 / H_mc_signal -> Integral() );
        H_data_sb -> Scale( 1 / H_data_sb -> Integral() );
        H_data_signal -> Scale( 1 / H_data_signal -> Integral() );

        draw_com3(H_mc_sb, string("mc_sideband"),
                  H_mc_signal, string("mc_signal"),
                  H_data_sb, string("data_sideband"),
                  Name2[i]);

        delete H_mc_sb;
        delete H_mc_signal;
        delete H_data_sb;
        delete H_data_signal;

    }

}



