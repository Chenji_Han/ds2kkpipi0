/*************************************************************************
    > File Name: preFit.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Fri 14 Feb 2020 01:19:14 PM CST
 ************************************************************************/

<<<<<<< HEAD
<<<<<<< HEAD
<<<<<<< HEAD
<<<<<<< HEAD
<<<<<<< HEAD
void preFit (string inputFileName=string("sample/signalMC1C.root")){
=======
void preFit (string inputFileName=string("sample/Jpsi.root")){
>>>>>>> parent of 5782c71... add lambda mass distri check
=======
void preFit (string inputFileName=string("sample/Jpsi.root")){
>>>>>>> parent of 5782c71... add lambda mass distri check
=======
void preFit (string inputFileName=string("sample/Jpsi.root")){
>>>>>>> parent of 5782c71... add lambda mass distri check
=======
void preFit (string inputFileName=string("sample/Jpsi.root")){
>>>>>>> parent of 5782c71... add lambda mass distri check
=======
void preFit (string inputFileName=string("sample/Jpsi.root")){
>>>>>>> parent of 5782c71... add lambda mass distri check

    TFile* inputFile = new TFile(inputFileName.c_str());
    TTree* inputTree = (TTree*) inputFile -> Get( "Xi" );

    size_t suffix = inputFileName.find_last_of(".");
    string preffix = inputFileName.substr(0,suffix);
    string outputFileName = preffix + string("_precut.root");

    ifstream f(outputFileName.c_str());
    if( f.good() ){
        cout<<"the primiary cut has been done"<<endl;
        return;
    }
    TFile* outputFile = new TFile(outputFileName.c_str();, "recreate" );
    TTree* outputTree = new TTree( "Xi", "");

    double mXip4;
    double mLbd;
    double mXiRecoil;
    double DE_Xi;
    int XiMatch;
    inputTree -> SetBranchAddress( "mXip4", &mXip4 );
    inputTree -> SetBranchAddress( "mLbd", &mLbd );
    inputTree -> SetBranchAddress( "mXiRecoil", &mXiRecoil );
    inputTree -> SetBranchAddress( "DE_Xi", &DE_Xi );
    inputTree -> SetBranchAddress( "XiMatch", &XiMatch );

    double mXi;
    outputTree -> Branch( "mXip4", &mXi );
    outputTree -> Branch( "XiMatch", &XiMatch );

    for(int i=0; i<inputTree->GetEntries(); i++){
        if(i%10000==0) cout<<i<<endl;

        inputTree -> GetEntry(i);

        //if(!(mLbd>1.109&&mLbd<1.123&&mXiRecoil>1.28&&mXiRecoil<1.36)) continue;
        //if(!(mLbd>1.112&&mLbd<1.120&&mXiRecoil>1.28&&mXiRecoil<1.36)) continue;
        //if(!(mLbd>1.110&&mLbd<1.122&&mXiRecoil>1.28&&mXiRecoil<1.36)) continue;
        if(!(mLbd>1.111&&mLbd<1.121&&mXiRecoil>1.30&&mXiRecoil<1.33)) continue;
//      if(!(mLbd>1.111&&mLbd<1.121&&DE_Xi>-0.02&&DE_Xi<0.02&&mXiRecoil>1.30&&mXiRecoil<1.34)) continue;

        mXi = mXip4 - mLbd + 1.115683;

        outputTree -> Fill();
    }

    outputFile -> cd(); 
    outputTree -> Write();
    outputFile -> Close(); 
    inputFile -> Close(); 


}
