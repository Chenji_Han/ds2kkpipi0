/*************************************************************************
    > File Name: checklbd.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Wed 01 Apr 2020 04:27:49 PM CST
 ************************************************************************/

#include"DrawHist.cxx"


void checkOriResolution() {

    TFile inputFile("sample/signalMC.root");
    TTree* inputTree = (TTree*) inputFile.Get("XiTruth");
    TTree* inputTree_reco = (TTree*) inputFile.Get("Xi");
  
    double reco_XiPX,  reco_XiPY,  reco_XiPZ ;
    double mLbd;

    inputTree_reco -> SetBranchAddress("mLbd", &mLbd);
    inputTree_reco -> SetBranchAddress("XiPX", &reco_XiPX);
    inputTree_reco -> SetBranchAddress("XiPY", &reco_XiPY);
    inputTree_reco -> SetBranchAddress("XiPZ", &reco_XiPZ);
   
    double ProtonPPX, ProtonPPY, ProtonPPZ ;
    double PionMXiPX, PionMXiPY, PionMXiPZ ;
    double PionMLbdPX, PionMLbdPY, PionMLbdPZ ;

    inputTree -> SetBranchAddress("ProtonPPX", &ProtonPPX);
    inputTree -> SetBranchAddress("ProtonPPY", &ProtonPPY);
    inputTree -> SetBranchAddress("ProtonPPZ", &ProtonPPZ);
    
    inputTree -> SetBranchAddress("PionMXiPX", &PionMXiPX);
    inputTree -> SetBranchAddress("PionMXiPY", &PionMXiPY);
    inputTree -> SetBranchAddress("PionMXiPZ", &PionMXiPZ);

    inputTree -> SetBranchAddress("PionMLbdPX", &PionMLbdPX);
    inputTree -> SetBranchAddress("PionMLbdPY", &PionMLbdPY);
    inputTree -> SetBranchAddress("PionMLbdPZ", &PionMLbdPZ);

    TH1D HX("HX","", 100, -0.1, 0.1); 
    TH1D HY("HY","", 100, -0.1, 0.1); 
    TH1D HZ("HZ","", 100, -0.1, 0.1); 

    const double mpion    = 0.13957;
    const double mproton  = 0.938272;

    for(int ievt=0; ievt<inputTree->GetEntries(); ievt++ ){
        inputTree -> GetEntry(ievt);
        inputTree_reco -> GetEntry(ievt);
    
        if(!(mLbd>1.111&&mLbd<1.121)) continue;

        double protonE = sqrt( ProtonPPX*ProtonPPX + 
                               ProtonPPY*ProtonPPY +
                               ProtonPPZ*ProtonPPZ +
                               mproton*mproton );

        double pionXiE = sqrt( PionMXiPX*PionMXiPX + 
                               PionMXiPY*PionMXiPY +
                               PionMXiPZ*PionMXiPZ +
                               mpion*mpion );

        double pionLbdE = sqrt( PionMLbdPX*PionMLbdPX + 
                                PionMLbdPY*PionMLbdPY +
                                PionMLbdPZ*PionMLbdPZ +
                                mpion*mpion );

        double XiPX = PionMLbdPX + ProtonPPX + PionMXiPX;
        double XiPY = PionMLbdPY + ProtonPPY + PionMXiPY;
        double XiPZ = PionMLbdPZ + ProtonPPZ + PionMXiPZ;
        double XiE  = pionLbdE   + protonE   + pionXiE;

        TLorentzVector lab(XiPX,XiPY,XiPZ,XiE);
        lab = lab.Boost(0.011,0,0);
        double px = lab.Px();
        double py = lab.Py();
        double pz = lab.Pz();
        
        HX.Fill(reco_XiPX - px);
        HY.Fill(reco_XiPY - py);
        HZ.Fill(reco_XiPZ - pz);
    }

    draw(&HX,string("PX of #Xi^{-} "),string("oripx") );
    draw(&HY,string("PY of #Xi^{-} "),string("oripy") );
    draw(&HZ,string("PZ of #Xi^{-} "),string("oripz") );

    return;
}





