/*************************************************************************
    > File Name: Bukin_extraction.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Thu 09 Apr 2020 02:15:02 PM CST
 ************************************************************************/

#include"bes3plotstyle.C"

using namespace RooFit ;

void setLatex(TLatex* latex,double size){
	
	latex->SetNDC();
    latex->SetTextFont(12);
    latex->SetTextSize(size);
    latex->SetTextAlign(33);
}


void Bukin_draw(string inputName=string("./sample/signalMC5C.root"),string storename=string("Bukin") ){

    TFile *inputFile = new TFile(inputName.c_str());
    TTree *tr = (TTree*)inputFile->Get("Xi");
 	
	RooRealVar mXi("mXip4","",-300,300);

    RooRealVar _Xp("_Xp","",20);
    RooRealVar _sigp("_sigp","",60);
    RooRealVar _xi("_xi","",-0.2);
    RooRealVar _rho1("_rho1","",-0.5);
    RooRealVar _rho2("_rho2","",0.1);

    RooBukinPdf model("model","",mXi, _Xp, _sigp, _xi, _rho1, _rho2 );
    
    TCanvas* C = new TCanvas("C","C",800,600);
    SetStyle();
    
    gPad->SetLeftMargin(0.18);
    gPad->SetBottomMargin(0.13);
    RooPlot* frame = mXi.frame(-300,300); 
    model.plotOn(frame,LineWidth(3),LineColor(kBlue)) ;   
    
    FormatAxis(frame->GetYaxis());
    FormatAxis(frame->GetXaxis());
    frame->GetYaxis()->SetRangeUser(0,frame->GetMaximum()*1.15);
    frame->GetXaxis()->SetTitle("");
    frame->GetYaxis()->SetTitle("");
    frame->GetYaxis()->SetLabelSize(0.03);
    frame->GetYaxis()->SetTitleSize(0.04);
    frame->GetXaxis()->SetLabelSize(0.03);
    frame->GetXaxis()->SetTitleSize(0.04);
	frame->Draw();

	TLatex * comment1 = new TLatex(0.85,0.78,"Xp=20");
	setLatex(comment1,0.037);
	comment1 -> Draw("same");

	TLatex * comment2 = new TLatex(0.85,0.73,"sigp=60");
	setLatex(comment2,0.037);
	comment2 -> Draw("same");

	TLatex * comment3 = new TLatex(0.85,0.68,"rho1=-0.5");
	setLatex(comment3,0.037);
	comment3 -> Draw("same");

	TLatex * comment4 = new TLatex(0.85,0.63,"rho2=0.1");
	setLatex(comment4,0.037);
	comment4 -> Draw("same");

    TLatex * comment5 = new TLatex(0.85,0.58,"xi=-0.2");
	setLatex(comment5,0.037);
	comment5 -> Draw("same");
    
    string saveName = string("./plots/") + storename + string(".png");
    string saveNameeps = string("./plots/") + storename + string(".eps");
    cout<<saveName<<endl;
	C->SaveAs(saveName.c_str());
	C->SaveAs(saveNameeps.c_str());
   
}


