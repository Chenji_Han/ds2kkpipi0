#########################################################################
# File Name: run.sh
# Author: Chenji Han
# mail: hanchenji16@mails.ucas.ac.cn
# Created Time: Tue 18 Feb 2020 02:36:55 PM CST
#########################################################################
#!/bin/bash

#export LC_ALL=en_US.UTF8
#
#boss704
#
#sh ./clean-runflow.sh

#hep_sub runflow.sh -argu M1.32000.root 1.32000
#hep_sub runflow.sh -argu signalMC.root 1.32132 
#hep_sub runflow.sh -argu M1.32200.root 1.32200
#hep_sub runflow.sh -argu M1.32300.root 1.32300
#hep_sub runflow.sh -argu M1.32500.root 1.32500

sh runflow.sh  M1.32000.root 1.32000
sh runflow.sh  signalMC.root 1.32132
sh runflow.sh  M1.32200.root 1.32200
sh runflow.sh  M1.32300.root 1.32300
sh runflow.sh  M1.32500.root 1.32500




