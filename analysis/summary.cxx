/*************************************************************************
    > File Name: summary.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Thu 20 Feb 2020 04:06:20 PM CST
 ************************************************************************/
#include"bes3plotstyle.C"
#include<iostream>
using namespace std;

double string2double(string input){
	
	int index = 0;
	double pre = 0.0;
	double aft = 0.0;
	bool comma = false;
	double num = 1.0;
	double charge;
	if(input[0]=='-') 
		charge = -1.0;
	else 
		charge = 1.0;

    bool scit = false;
    double suffix = 0;
    double factor = 0;

	while(input[index]!='\0'){
		if(input[index]=='.'){
			comma = true;
		}
		if('0'<=input[index]&&input[index]<='9'){
			double temp = input[index] - '0';
			if(comma==false){
				pre = pre*10.0 + temp;
			}else{
				num *= 10.0;
				aft += temp/num;
			}
		}
        if(input[index]=='e' || input[index] == 'E'){
            scit = true;
            suffix = pre + aft;
            factor = string2double( input.substr( size_t(index+1) ) );
            break; 
        }
		index++;
	}

    if(scit)
	    return charge * suffix * pow( 10, factor ) ;
    else
        return charge * ( pre + aft) ;
}



int readTxt (double X[], double Y[], double Y_Error[]) {

    string a; // mass tag
    string b; // 1.32132 + gauss mean
    string c; // error of gauss mean

    string d; // sigma of gauss 
    string e; // error of sigma of gauss 

    string f; // mean of generated data set
    string g; // error of mean of generated data set

    string h; // sigma of generated data set 
    string m; // error of sigma of generated data set 

    string n; // number of signal events

    int ievt = 0;
    std::fstream inputTxtFile("output.txt",ios::in);
    string line;
    while(std::getline(inputTxtFile,line)){
        if( !line.length() || line[0]=='#' ){
            continue;
        }
        std::istringstream iss(line);
        iss>>a>>b>>c>>d>>e>>f>>g>>h>>m>>n;

        // X -> a
        // Y -> b
        // Y Error -> h/sqrt(n) 
        X[ievt] = string2double(a);
        Y[ievt] = string2double(b);
        Y_Error[ievt] =  string2double(h) / sqrt( string2double(n) ) ;
        cout<<X[ievt]<<"  "<<Y[ievt]<<"  "<<Y_Error[ievt]<<"  "<<endl; 
        ievt++;
    }

    return ievt;
}


void summary (){

    double X[14];
    double Y[14];
    double Y_Error[14];
    double X_Error[14];

    for(int i=0;i<14;i++) X_Error[i] = 0;

    int ievt = readTxt(X,Y,Y_Error);
    TGraphErrors* graph = new TGraphErrors( ievt, X, Y, X_Error, Y_Error );

    TCanvas C("c", "c", 800, 600);
    SetStyle();
    FormatAxis( graph -> GetXaxis() );
    FormatAxis( graph -> GetYaxis() );
    graph -> GetXaxis() -> SetTitle( " mass that recoil side is constrained to " );
    graph -> GetXaxis() -> SetTitleSize( 0.04 );
    graph -> GetXaxis() -> SetLabelSize( 0.04 );
    graph -> GetYaxis() -> SetTitle( " fitting result( 1.32132 + mean(gauss) ) ");
    graph -> GetYaxis() -> SetTitleSize( 0.04 );
    graph -> GetYaxis() -> SetTitleOffset( 1.65 );
    graph -> GetYaxis() -> SetLabelOffset( 0.005 );
    graph -> GetYaxis() -> SetLabelSize( 0.04 );
    graph -> Draw("AP");
    gPad -> SetLeftMargin( 0.25 );
    gPad -> SetBottomMargin( 0.2 );
    C.SaveAs("recoilSummarynew.png");


}













