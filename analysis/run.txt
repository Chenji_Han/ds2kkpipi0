#include "$ROOTIOROOT/share/jobOptions_ReadRoot.txt"
#include "$MAGNETICFIELDROOT/share/MagneticField.txt" 
#include "$ABSCORROOT/share/jobOptions_AbsCor.txt"
#include "$VERTEXFITROOT/share/jobOptions_VertexDbSvc.txt" 
#include "$DTAGALGROOT/share/jobOptions_dTag.txt" 

#include "/besfs/users/hancj/boss-7.0.3/workarea/Ds2KKPiPi0Alg/Ds2KKPiPi0-master/share/jobOptions_Ds2KKPiPi0.txt"


//Input REC or DST file name 
//EventCnvSvc.digiRootInputFile = { "/besfs3/offline/data/703-1/4180/mc/01/dst/DsSTDs/DsSTDs_round01_002_IHEP.dst"};
//EventCnvSvc.digiRootInputFile = { "/afs/ihep.ac.cn/users/h/hancj/scratchfs/DsMC/Ds2KsK_dtag/dst/dst_0002.dst"};
//EventCnvSvc.digiRootInputFile = { "/afs/ihep.ac.cn/users/h/hancj/scratchfs/DsMC/STV4/dst/dst_0002.dst"};
//EventCnvSvc.digiRootInputFile = { "/afs/ihep.ac.cn/users/h/hancj/scratchfs/MC_Jpsi2XiXibar/dst/dst_0003.dst"};
EventCnvSvc.digiRootInputFile = { "/afs/ihep.ac.cn/users/h/hancj/workfs/DATA/DsMCdst/dst_0001.dst"};

// Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc.OutputLevel = 5;

// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = 10000;



