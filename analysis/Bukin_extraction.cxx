/*************************************************************************
    > File Name: Bukin_extraction.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Thu 09 Apr 2020 02:15:02 PM CST
 ************************************************************************/

#include"bes3plotstyle.C"

using namespace RooFit ;

void Bukin_extraction(string inputName=string("./sample/signalMC5C.root"),string storename=string("BukinExtractionMC") ){

    TFile *inputFile = new TFile(inputName.c_str());
    TTree *tr = (TTree*)inputFile->Get("Xi");
 	
	RooRealVar mXi("mXip4","",1.31,1.335);

    Int_t nentrs = tr->GetEntries();
  
    RooRealVar _Xp("_Xp","",1.32,1.324);
    RooRealVar _sigp("_sigp","",0.0005,0.01);
    RooRealVar _xi("_xi","",-2,2);
    RooRealVar _rho1("_rho1","",-5,5);
    RooRealVar _rho2("_rho2","",-10,5);

    RooBukinPdf model("model","",mXi, _Xp, _sigp, _xi, _rho1, _rho2 );
    
    RooDataSet data("data","data",tr,mXi);

    RooFitResult * result = model.fitTo(data,Save(kTRUE));

    ofstream outputTxtFile(".BukinExtractionMC.txt",ios::out);
    if( !outputTxtFile ){
        cout<<" Error in opening output.txt " <<endl;
        return;
    }
   	outputTxtFile<<_Xp.getValV()<<endl;
   	outputTxtFile<<_sigp.getValV()<<endl;
   	outputTxtFile<<_xi.getValV()<<endl;
   	outputTxtFile<<_rho1.getValV()<<endl;
   	outputTxtFile<<_rho2.getValV()<<endl;
    outputTxtFile.close();
 

    TCanvas* C = new TCanvas("C","C",800,600);
    SetStyle();

    gPad->SetLeftMargin(0.18);
    gPad->SetBottomMargin(0.13);
    RooPlot* frame = mXi.frame(1.31,1.335,150); 
	data.plotOn(frame,MarkerSize(0.6)); 
    model.plotOn(frame,LineWidth(3),LineColor(kBlue)) ;   

    FormatAxis(frame->GetYaxis());
    FormatAxis(frame->GetXaxis());
    frame->GetYaxis()->SetRangeUser(0.1,6000);
    frame->GetXaxis()->SetTitle("mass(#Xi^{-})/GeV");
    frame->GetYaxis()->SetTitle("Events");
    frame->GetYaxis()->SetLabelSize(0.03);
    frame->GetYaxis()->SetTitleSize(0.04);
    frame->GetXaxis()->SetLabelSize(0.03);
    frame->GetXaxis()->SetTitleSize(0.04);
	frame->Draw();
    
    string saveName = string("./plots/") + storename + string(".png");
    string saveNameeps = string("./plots/") + storename + string(".eps");
    cout<<saveName<<endl;
	C->SaveAs(saveName.c_str());
	C->SaveAs(saveNameeps.c_str());
    
    TCanvas* C2 = new TCanvas("C2","C2",800,600);
    frame->GetYaxis()->SetRangeUser(0.1,1e5);
    gPad->SetLogy();
    frame -> Draw();
    string saveName2 = string("./plots/") + storename + string("_logy.png");
    string saveName2eps = string("./plots/") + storename + string("_logy.eps");
    cout<<saveName2<<endl;
    C2 -> SaveAs(saveName2.c_str());
    C2 -> SaveAs(saveName2eps.c_str());
    
	Int_t ndf = result->floatParsFinal().getSize();
	Double_t chisq = frame->chiSquare();
	Double_t reduced_chisq = frame->chiSquare(ndf);
	cout << "NDF:" << ndf << endl;
	cout << "chi square: " << chisq <<endl;
	cout << "reduced chi square: " << reduced_chisq << endl;
    cout <<scientific<<"fitted mass: "<<_Xp.getValV()<<" +/- "<<_Xp.getError()<<endl;
    cout <<scientific<<(_Xp.getValV()-_Xp.getError())<<" ~ "<<(_Xp.getValV()+_Xp.getError())<<endl;




}
