/*************************************************************************
    > File Name: Tracks.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Mon 29 Jun 2020 11:19:22 PM CST
 ************************************************************************/


#include "Ds2KKPiPi0Alg/Ds2KKPiPi0.h" // header file

bool _EPID(EvtRecTrack* trk){

    ParticleID * pid=ParticleID::instance();
	
    double m_prob_pid_pi,m_prob_pid_k,m_prob_pid_e;
	pid->init();
	pid->setMethod(pid->methodProbability());
	pid->setChiMinCut(4);
	pid->setRecTrack(trk);
	pid->usePidSys( pid->useDedx() | pid->useTofCorr() ); // use PID sub-system
	pid->identify( pid->onlyPion() | pid->onlyKaon() | pid->onlyElectron() );    // seperater Pion/Kaon/Proton
	pid->calculate();
	
    if( !( pid->IsPidInfoValid() ) ){
        return false;
    }else{
	    m_prob_pid_k  = pid->probKaon();
	    m_prob_pid_e  = pid->probElectron();
	    m_prob_pid_pi = pid->probPion();
    }

    double Q = m_prob_pid_e / (m_prob_pid_pi + m_prob_pid_k + m_prob_pid_e);
    if( !(Q<0.8 || m_prob_pid_e<m_prob_pid_pi || m_prob_pid_e<m_prob_pid_k) ){
        return true;
    }else{
        return false;
    }

}


bool _PionPID(EvtRecTrack* trk){

    ParticleID * pid=ParticleID::instance();
	
    double m_prob_pid_pi,m_prob_pid_k;
	pid->init();
	pid->setMethod(pid->methodProbability());
	pid->setChiMinCut(4);
	pid->setRecTrack(trk);
	pid->usePidSys( pid->useDedx() | pid->useTofCorr() ); // use PID sub-system
	pid->identify( pid->onlyPion() | pid->onlyKaon() );    // seperater Pion/Kaon/Proton
	pid->calculate();
	if( !( pid->IsPidInfoValid() ) )
    {
    	m_prob_pid_k  = -999 ;
	    m_prob_pid_pi = -999 ; 
    }
    else
    {
	    m_prob_pid_k  = pid->probKaon();
	    m_prob_pid_pi = pid->probPion();
    }

    if(m_prob_pid_pi>0 && m_prob_pid_pi>m_prob_pid_k){
        return true;
    }else{
        return false;
    }

}

bool _KaonPID(EvtRecTrack* trk){

    ParticleID * pid=ParticleID::instance();
	
    double m_prob_pid_pi,m_prob_pid_k;
	pid->init();
	pid->setMethod(pid->methodProbability());
	pid->setChiMinCut(4);
	pid->setRecTrack(trk);
	pid->usePidSys( pid->useDedx() | pid->useTofCorr() ); // use PID sub-system
	pid->identify( pid->onlyPion() | pid->onlyKaon() );    // seperater Pion/Kaon/Proton
	pid->calculate();
	if( !( pid->IsPidInfoValid() ) )
    {
    	m_prob_pid_k  = -999 ;
	    m_prob_pid_pi = -999 ; 
    }
    else
    {
	    m_prob_pid_k  = pid->probKaon();
	    m_prob_pid_pi = pid->probPion();
    }

    if(m_prob_pid_k>0 && m_prob_pid_k>m_prob_pid_pi){
        return true;
    }else{
        return false;
    }


}



void Ds2KKPiPi0::IsGoodTrack(SmartDataPtr<EvtRecEvent> evtRecEvent,SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,vector<HepLorentzVector>& PionP,vector<int>& PionPIndex,  vector<HepLorentzVector>& PionM, vector<int>& PionMIndex, vector<HepLorentzVector>& KaonP, vector<int>& KaonPIndex, vector<HepLorentzVector>& KaonM, vector<int>& KaonMIndex,vector<HepLorentzVector>& ElectronP,vector<int>& ElectronPIndex,vector<HepLorentzVector>& ElectronM,vector<int>& ElectronMIndex){

	Hep3Vector xorigin(0,0,0);

	IVertexDbSvc*  vtxsvc;
	Gaudi::svcLocator()->service("VertexDbSvc", vtxsvc);
	if(vtxsvc->isVertexValid()){
		double* dbv = vtxsvc->PrimaryVertex(); 
        double*  vv = vtxsvc->SigmaPrimaryVertex();
		xorigin.setX(dbv[0]);
		xorigin.setY(dbv[1]);
		xorigin.setZ(dbv[2]);
	}else{
		cout<<"invalid vertex"<<endl;
		return;
	}

	for(int i = 0; i < evtRecEvent->totalCharged(); i++){

		EvtRecTrackIterator itTrk=evtRecTrkCol->begin() + i;
		
		if(!(*itTrk)->isMdcTrackValid()) continue;
		if(!(*itTrk)->isMdcKalTrackValid()) continue;

		RecMdcTrack *mdcTrk = (*itTrk)->mdcTrack();
		HepVector a = mdcTrk->helix();
		HepSymMatrix Ea = mdcTrk->err();
		HepPoint3D point0(0.,0.,0.);   // the initial point for MDC recosntruction
		HepPoint3D IP(xorigin[0],xorigin[1],xorigin[2]); 
		VFHelix helixip(point0,a,Ea); 
		helixip.pivot(IP);
		HepVector vecipa = helixip.a();
		double  Rvxy0=fabs(vecipa[0]);  //the nearest distance to IP in xy plane
		double  Rvz0=vecipa[3];         //the nearest distance to IP in z direction

		double costheta = cos( mdcTrk->theta() );
		if(!(abs(costheta)<0.93)) continue;

        if(fabs(Rvz0) >= 10.0) continue;
        if(fabs(Rvxy0) >= 1.0) continue;

		RecMdcKalTrack* mdcKalTrk = (*itTrk)->mdcKalTrack();

        if(_KaonPID(*itTrk)){

			mdcKalTrk->setPidType(RecMdcKalTrack::kaon);
			
			int charge_temp = mdcKalTrk->charge();
			HepLorentzVector temp = mdcKalTrk -> p4(mk);
            //temp.boost(-0.011,0,0);
			
			if (charge_temp==1){
		        KaonP.push_back(temp);
                KaonPIndex.push_back(i);
			}else if(charge_temp==-1){
		 		KaonM.push_back(temp);
                KaonMIndex.push_back(i);
			}
		
		}
 
        if(_PionPID(*itTrk)){

			mdcKalTrk->setPidType(RecMdcKalTrack::pion);
			
			int charge_temp = mdcKalTrk->charge();
			HepLorentzVector temp = mdcKalTrk -> p4(mpi);
            //temp.boost(-0.011,0,0);
			
			if (charge_temp==1){
		        PionP.push_back(temp);
                PionPIndex.push_back(i);
			}else if(charge_temp==-1){
		 		PionM.push_back(temp);
                PionMIndex.push_back(i);
			}
		
		}

        if(_EPID(*itTrk)){	

            mdcKalTrk->setPidType(RecMdcKalTrack::electron);
			
			int charge_temp = mdcKalTrk->charge();
			HepLorentzVector temp = mdcKalTrk -> p4(me);
			
			if (charge_temp==1){
		        ElectronP.push_back(temp);
                ElectronPIndex.push_back(i);
			}else if(charge_temp==-1){
		 		ElectronM.push_back(temp);
                ElectronMIndex.push_back(i);
			}

        }


	}

    return;

}









