/*************************************************************************
    > File Name: IsPi0.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Sat 11 Apr 2020 02:51:43 PM CST
 ************************************************************************/

#include "Ds2KKPiPi0Alg/Ds2KKPiPi0.h"

void Ds2KKPiPi0::IsGammatrack(SmartDataPtr<EvtRecEvent> evtRecEvent, SmartDataPtr<EvtRecTrackCol> evtRecTrkCol, vector<int>& Gam_Track, vector<HepLorentzVector>& Gam_P4,vector<double>& Gam_dangle){

	Hep3Vector xorigin(0,0,0);
	IVertexDbSvc*  vtxsvc;
	Gaudi::svcLocator()->service("VertexDbSvc", vtxsvc);
	if(vtxsvc->isVertexValid()){
		double* dbv = vtxsvc->PrimaryVertex(); 
		double*  vv = vtxsvc->SigmaPrimaryVertex();  
		xorigin.setX(dbv[0]);
		xorigin.setY(dbv[1]);
		xorigin.setZ(dbv[2]);
	}else{
		return;
	}

	for(int i = evtRecEvent->totalCharged(); i< evtRecEvent->totalTracks(); i++){

    	EvtRecTrackIterator itTrk=evtRecTrkCol->begin() + i;
    	if(!(*itTrk)->isEmcShowerValid()) continue;

        RecEmcShower *emcTrk = (*itTrk)->emcShower();

        double detecttime=emcTrk->time();
    	if( detecttime>14.0 || detecttime<0.0 ) continue;//time cut
    
    	Hep3Vector Gm_Vec(emcTrk->x(), emcTrk->y(), emcTrk->z());
    	Hep3Vector Gm_Mom = Gm_Vec - xorigin;
    	Gm_Mom.setMag(emcTrk->energy());
    	HepLorentzVector Gam_EP(Gm_Mom, emcTrk->energy());

    	double eraw = emcTrk->energy();
    	double costheta=Gam_EP.vect().cosTheta();
    	if(fabs(costheta)<0.8){
    		if(!(eraw>0.025)) continue;
        }
    	if(0.86<fabs(costheta)&&fabs(costheta)<0.92){
    		if(!(eraw>0.05)) continue; 
        }

        double DeltaAngle = -999;
        for(int j = 0; j < evtRecEvent->totalCharged(); j++){
            EvtRecTrackIterator ijTrk = evtRecTrkCol->begin() + j;

            if(!(*ijTrk)->isMdcTrackValid()) continue;
		    if(!(*ijTrk)->isMdcKalTrackValid()) continue;

		    RecMdcTrack *mdcTrk = (*ijTrk)->mdcTrack();
            HepVector a = mdcTrk->helix();
		    HepSymMatrix Ea = mdcTrk->err();
		    HepPoint3D point0(0.,0.,0.);   // the initial point for MDC recosntruction
		    HepPoint3D IP(xorigin[0],xorigin[1],xorigin[2]); 
		    VFHelix helixip(point0,a,Ea); 
		    helixip.pivot(IP);
		    HepVector vecipa = helixip.a();
		    double  Rvxy0=fabs(vecipa[0]);  //the nearest distance to IP in xy plane
		    double  Rvz0=vecipa[3];         //the nearest distance to IP in z direction
            
            if(fabs(Rvz0) >= 10.0) continue;
            if(fabs(Rvxy0) >= 1.0) continue;
		    double costheta = cos( mdcTrk->theta() );
		    if(!(abs(costheta)<0.93)) continue;

            if(!(*ijTrk)->isExtTrackValid()) continue;
            RecExtTrack *extTrk = (*ijTrk)->extTrack();
            if(extTrk->emcVolumeNumber() == -1) continue;
            Hep3Vector extpos = extTrk->emcPosition();
            double angd = extpos.angle(Gm_Vec);
            angd = angd * 180 / (CLHEP::pi);
        //    cout<<"delta angle "<<angd<<endl;

            if( abs(angd) < abs(DeltaAngle) ){
                DeltaAngle = angd;
            }
            
        }

		Gam_Track.push_back(i);
        //Gam_EP.boost(-0.011,0,0);
        Gam_P4.push_back(Gam_EP);
        Gam_dangle.push_back(DeltaAngle);


	}


}	


void Ds2KKPiPi0::IsPi0(SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,vector<HepLorentzVector> Gam_EP,vector<int> Gam_Track,vector<double> Gam_dangle,vector<HepLorentzVector>& Pi0_P4, double Pi0_shower[50][6]){

	int nGam=Gam_Track.size();
    if(nGam<2) return;

    int num = 0;
	for(int i = 0; i < nGam-1; i++) {

		for(int j = i+1; j < nGam; j++) {

			HepLorentzVector Pi0_ep;
			Pi0_ep=Gam_EP[i] + Gam_EP[j];
			double pi0_mass=Pi0_ep.m();

			if(!(0.115<pi0_mass && pi0_mass< 0.150))  continue;
            //cout<<"mass of pi0 candidate "<<pi0_mass<<endl;

			EvtRecTrackIterator itTrki = evtRecTrkCol->begin() + Gam_Track[i];
			RecEmcShower* shr1 = (*itTrki)->emcShower();
			EvtRecTrackIterator itTrkj = evtRecTrkCol->begin() + Gam_Track[j];
			RecEmcShower* shr2 = (*itTrkj)->emcShower();
        
        	KalmanKinematicFit * kmfit = KalmanKinematicFit::instance();
        	kmfit->init();
        	kmfit->AddTrack(0, 0.0, shr1);
        	kmfit->AddTrack(1, 0.0, shr2);
        	kmfit->AddResonance(0, 0.134976, 0, 1);
        
        	if( !(kmfit->Fit()) ) continue;
            double chisq = kmfit->chisq();
        
        	kmfit->BuildVirtualParticle(0);
        	HepLorentzVector p4_pi0 = kmfit->pfit(0) + kmfit->pfit(1);

            //p4_pi0.boost(-0.011,0,0);
            Pi0_P4.push_back(p4_pi0);
            Pi0_shower[num][0]  = Gam_Track[i];
            Pi0_shower[num][1]  = Gam_Track[j];
            Pi0_shower[num][2]  = pi0_mass;
            Pi0_shower[num][3]  = Gam_dangle[i];
            Pi0_shower[num][4]  = Gam_dangle[j];
            Pi0_shower[num][5]  = chisq;
            num ++;

		}
	}


}






