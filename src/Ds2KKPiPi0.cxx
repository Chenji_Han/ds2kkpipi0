

#include "Ds2KKPiPi0Alg/Ds2KKPiPi0.h"

double Ds2KKPiPi0::_PT(const HepLorentzVector& P){

    return sqrt( P.px()*P.px() + P.py()*P.py() );

}

double Ds2KKPiPi0::_P(const HepLorentzVector& P){

    return sqrt( P.px()*P.px() + P.py()*P.py() + P.pz()*P.pz() );

}

double Ds2KKPiPi0::_M(const HepLorentzVector& P){

    return P.m();

}

double Ds2KKPiPi0::_recoilE4DsS(const HepLorentzVector& P){

    return sqrt( P.px()*P.px() + P.py()*P.py() + P.pz()*P.pz() + mDs*mDs );

}
double Ds2KKPiPi0::_recoilMass(const HepLorentzVector& P){

    HepLorentzVector temp = HepLorentzVector(0,0,0,4.178) - HepLorentzVector( P.px(), P.py(), P.pz(), _recoilE4DsS(P) );
    return temp.m();

}
///////////////////////////////////////////////////////////////

Ds2KKPiPi0::Ds2KKPiPi0(const std::string& name, ISvcLocator* pSvcLocator) :
  Algorithm(name, pSvcLocator){

}

StatusCode Ds2KKPiPi0::initialize(){

    MsgStream log(msgSvc(), name());
    
    log << MSG::INFO << "in initialize()" << endmsg;
  
    // next few lines of code are used to read some parameters fromt the inputs file
    string outputName;
    string line;
    std::ifstream inputTxt("inputs.txt");
    while(getline(inputTxt, line)) {
            if (!line.length() || line[0] == '#')
                continue;
    
        std::istringstream iss(line);
            string a,b,c;
            iss>>a>>b>>c;
            if(a=="dst_outputName"){
                outputName = b;
            }else if(a=="dst_angleMatch"){
                angleMatch_switch = b;
            }
    
    }

    //next codes are used to initialize the match service
    //IMCTruthMatchSvc *i_matchSvc;
    //StatusCode sc_MC = service("MCTruthMatchSvc",i_matchSvc);
    //if(sc_MC.isFailure()){
    //	cout<<"MCTruthMatchSvc load fail"<<endl;
    //	return 0;
    //}
    //matchSvc=dynamic_cast<MCTruthMatchSvc*>(i_matchSvc);

    for(int i=0;i<100;i++){
        m_pdgid[i]=0;
        m_motheridx[i]=0;
    }

    m_numParticle = 0;    

    outputFile = new TFile(outputName.c_str(),"recreate");
    
    outputTree = new TTree("output","");
    outputTree_truth = new TTree("output_truth","");

    outputTree_truth -> Branch("indexmc",&m_numParticle);
    outputTree_truth -> Branch("pdgid",m_pdgid,"pdgid[100]/I");
    outputTree_truth -> Branch("motheridx",m_motheridx,"motheridx[100]/I");
    outputTree_truth -> Branch("runNumber",&runNumber);
    outputTree_truth -> Branch("eventNumber",&eventNumber);

    outputTree_truth -> Branch("K1_px_truth",&K1_px_truth);
    outputTree_truth -> Branch("K1_py_truth",&K1_py_truth);
    outputTree_truth -> Branch("K1_pz_truth",&K1_pz_truth);
    outputTree_truth -> Branch("K1_pt_truth",&K1_pt_truth);
    outputTree_truth -> Branch("K1_p_truth", &K1_p_truth);
    outputTree_truth -> Branch("K1_e_truth", &K1_e_truth);

    outputTree_truth -> Branch("K2_px_truth",&K2_px_truth);
    outputTree_truth -> Branch("K2_py_truth",&K2_py_truth);
    outputTree_truth -> Branch("K2_pz_truth",&K2_pz_truth);
    outputTree_truth -> Branch("K2_pt_truth",&K2_pt_truth);
    outputTree_truth -> Branch("K2_p_truth", &K2_p_truth);
    outputTree_truth -> Branch("K2_e_truth", &K2_e_truth);

    outputTree_truth -> Branch("Pi_px_truth",&Pi_px_truth);
    outputTree_truth -> Branch("Pi_py_truth",&Pi_py_truth);
    outputTree_truth -> Branch("Pi_pz_truth",&Pi_pz_truth);
    outputTree_truth -> Branch("Pi_pt_truth",&Pi_pt_truth);
    outputTree_truth -> Branch("Pi_p_truth", &Pi_p_truth);
    outputTree_truth -> Branch("Pi_e_truth", &Pi_e_truth);

    outputTree_truth -> Branch("otherKp_px_truth",&otherKp_px_truth);
    outputTree_truth -> Branch("otherKp_py_truth",&otherKp_py_truth);
    outputTree_truth -> Branch("otherKp_pz_truth",&otherKp_pz_truth);
    outputTree_truth -> Branch("otherKp_pt_truth",&otherKp_pt_truth);
    outputTree_truth -> Branch("otherKp_p_truth", &otherKp_p_truth);
    outputTree_truth -> Branch("otherKp_e_truth", &otherKp_e_truth);

    outputTree_truth -> Branch("otherKm_px_truth",&otherKm_px_truth);
    outputTree_truth -> Branch("otherKm_py_truth",&otherKm_py_truth);
    outputTree_truth -> Branch("otherKm_pz_truth",&otherKm_pz_truth);
    outputTree_truth -> Branch("otherKm_pt_truth",&otherKm_pt_truth);
    outputTree_truth -> Branch("otherKm_p_truth", &otherKm_p_truth);
    outputTree_truth -> Branch("otherKm_e_truth", &otherKm_e_truth);


    outputTree_truth -> Branch("otherPip_px_truth",&otherPip_px_truth);
    outputTree_truth -> Branch("otherPip_py_truth",&otherPip_py_truth);
    outputTree_truth -> Branch("otherPip_pz_truth",&otherPip_pz_truth);
    outputTree_truth -> Branch("otherPip_pt_truth",&otherPip_pt_truth);
    outputTree_truth -> Branch("otherPip_p_truth", &otherPip_p_truth);
    outputTree_truth -> Branch("otherPip_e_truth", &otherPip_e_truth);

    outputTree_truth -> Branch("otherPim_px_truth",&otherPim_px_truth);
    outputTree_truth -> Branch("otherPim_py_truth",&otherPim_py_truth);
    outputTree_truth -> Branch("otherPim_pz_truth",&otherPim_pz_truth);
    outputTree_truth -> Branch("otherPim_pt_truth",&otherPim_pt_truth);
    outputTree_truth -> Branch("otherPim_p_truth", &otherPim_p_truth);
    outputTree_truth -> Branch("otherPim_e_truth", &otherPim_e_truth);


    outputTree -> Branch("indexmc",&m_numParticle);
    outputTree -> Branch("pdgid",m_pdgid,"pdgid[100]/I");
    outputTree -> Branch("motheridx",m_motheridx,"motheridx[100]/I");
    outputTree -> Branch("runNumber",&runNumber);
    outputTree -> Branch("eventNumber",&eventNumber);
   
    outputTree -> Branch("charge",&charge);
    outputTree -> Branch("mode1",&mode1);
    outputTree -> Branch("mode2",&mode2);
    outputTree -> Branch("NKp",&NKp);
    outputTree -> Branch("NKm",&NKm);
    outputTree -> Branch("NPip",&NPip);
    outputTree -> Branch("NPim",&NPim);
    outputTree -> Branch("NPi0",&NPi0);
    outputTree -> Branch("NEp",&NEp);
    outputTree -> Branch("NEm",&NEm);
    outputTree -> Branch("NMup",&NMup);
    outputTree -> Branch("NMum",&NMum);
    
    outputTree -> Branch("Chisq1C",&Chisq);
    outputTree -> Branch("ChisqVF",&_ChisqVF);
    outputTree -> Branch("DeltaE",&DeltaE);
   
    outputTree -> Branch("K1_px",&K1_px);
    outputTree -> Branch("K1_py",&K1_py);
    outputTree -> Branch("K1_pz",&K1_pz);
    outputTree -> Branch("K1_pt",&K1_pt);
    outputTree -> Branch("K1_p", &K1_p);
    outputTree -> Branch("K1_e", &K1_e);
    outputTree -> Branch("K1_m", &K1_m);

    outputTree -> Branch("K2_px",&K2_px);
    outputTree -> Branch("K2_py",&K2_py);
    outputTree -> Branch("K2_pz",&K2_pz);
    outputTree -> Branch("K2_pt",&K2_pt);
    outputTree -> Branch("K2_p", &K2_p);
    outputTree -> Branch("K2_e", &K2_e);
    outputTree -> Branch("K2_m", &K2_m);

    outputTree -> Branch("Pi_px",&Pi_px);
    outputTree -> Branch("Pi_py",&Pi_py);
    outputTree -> Branch("Pi_pz",&Pi_pz);
    outputTree -> Branch("Pi_pt",&Pi_pt);
    outputTree -> Branch("Pi_p", &Pi_p);
    outputTree -> Branch("Pi_e", &Pi_e);
    outputTree -> Branch("Pi_m", &Pi_m);

    outputTree -> Branch("Pi0_px",&Pi0_px);
    outputTree -> Branch("Pi0_py",&Pi0_py);
    outputTree -> Branch("Pi0_pz",&Pi0_pz);
    outputTree -> Branch("Pi0_pt",&Pi0_pt);
    outputTree -> Branch("Pi0_p", &Pi0_p);
    outputTree -> Branch("Pi0_e", &Pi0_e);
    outputTree -> Branch("Pi0_m", &Pi0_m);
    outputTree -> Branch("Pi0_dangle1", &Pi0_dangle1);
    outputTree -> Branch("Pi0_dangle2", &Pi0_dangle2);
    outputTree -> Branch("Pi0_Chisq", &Pi0_Chisq);

    outputTree -> Branch("K1K2_px",&K1K2_px);
    outputTree -> Branch("K1K2_py",&K1K2_py);
    outputTree -> Branch("K1K2_pz",&K1K2_pz);
    outputTree -> Branch("K1K2_pt",&K1K2_pt);
    outputTree -> Branch("K1K2_p", &K1K2_p);
    outputTree -> Branch("K1K2_e", &K1K2_e);
    outputTree -> Branch("K1K2_m", &K1K2_m);

    outputTree -> Branch("K1Pi_px",&K1Pi_px);
    outputTree -> Branch("K1Pi_py",&K1Pi_py);
    outputTree -> Branch("K1Pi_pz",&K1Pi_pz);
    outputTree -> Branch("K1Pi_pt",&K1Pi_pt);
    outputTree -> Branch("K1Pi_p", &K1Pi_p);
    outputTree -> Branch("K1Pi_e", &K1Pi_e);
    outputTree -> Branch("K1Pi_m", &K1Pi_m);

    outputTree -> Branch("K2Pi_px",&K2Pi_px);
    outputTree -> Branch("K2Pi_py",&K2Pi_py);
    outputTree -> Branch("K2Pi_pz",&K2Pi_pz);
    outputTree -> Branch("K2Pi_pt",&K2Pi_pt);
    outputTree -> Branch("K2Pi_p", &K2Pi_p);
    outputTree -> Branch("K2Pi_e", &K2Pi_e);
    outputTree -> Branch("K2Pi_m", &K2Pi_m);

    outputTree -> Branch("PiPi0_px",&PiPi0_px);
    outputTree -> Branch("PiPi0_py",&PiPi0_py);
    outputTree -> Branch("PiPi0_pz",&PiPi0_pz);
    outputTree -> Branch("PiPi0_pt",&PiPi0_pt);
    outputTree -> Branch("PiPi0_p", &PiPi0_p);
    outputTree -> Branch("PiPi0_e", &PiPi0_e);
    outputTree -> Branch("PiPi0_m", &PiPi0_m);

    outputTree -> Branch("K1Pi0_px",&K1Pi0_px);
    outputTree -> Branch("K1Pi0_py",&K1Pi0_py);
    outputTree -> Branch("K1Pi0_pz",&K1Pi0_pz);
    outputTree -> Branch("K1Pi0_pt",&K1Pi0_pt);
    outputTree -> Branch("K1Pi0_p", &K1Pi0_p);
    outputTree -> Branch("K1Pi0_e", &K1Pi0_e);
    outputTree -> Branch("K1Pi0_m", &K1Pi0_m);

    outputTree -> Branch("K2Pi0_px",&K2Pi0_px);
    outputTree -> Branch("K2Pi0_py",&K2Pi0_py);
    outputTree -> Branch("K2Pi0_pz",&K2Pi0_pz);
    outputTree -> Branch("K2Pi0_pt",&K2Pi0_pt);
    outputTree -> Branch("K2Pi0_p", &K2Pi0_p);
    outputTree -> Branch("K2Pi0_e", &K2Pi0_e);
    outputTree -> Branch("K2Pi0_m", &K2Pi0_m);

    outputTree -> Branch("Gamma_px",&Gamma_px);
    outputTree -> Branch("Gamma_py",&Gamma_py);
    outputTree -> Branch("Gamma_pz",&Gamma_pz);
    outputTree -> Branch("Gamma_pt",&Gamma_pt);
    outputTree -> Branch("Gamma_p", &Gamma_p);
    outputTree -> Branch("Gamma_e", &Gamma_e);
    outputTree -> Branch("Gamma_m", &Gamma_m);

    outputTree -> Branch("Ds_px",&Ds_px);
    outputTree -> Branch("Ds_py",&Ds_py);
    outputTree -> Branch("Ds_pz",&Ds_pz);
    outputTree -> Branch("Ds_pt",&Ds_pt);
    outputTree -> Branch("Ds_p", &Ds_p);
    outputTree -> Branch("Ds_e", &Ds_e);
    outputTree -> Branch("Ds_m", &Ds_m);

    outputTree -> Branch("DsRecoil_px",&DsRecoil_px);
    outputTree -> Branch("DsRecoil_py",&DsRecoil_py);
    outputTree -> Branch("DsRecoil_pz",&DsRecoil_pz);
    outputTree -> Branch("DsRecoil_pt",&DsRecoil_pt);
    outputTree -> Branch("DsRecoil_p", &DsRecoil_p);
    outputTree -> Branch("DsRecoil_e", &DsRecoil_e);
    outputTree -> Branch("DsRecoil_m", &DsRecoil_m);

    outputTree -> Branch("DsGamma_px",&DsGamma_px);
    outputTree -> Branch("DsGamma_py",&DsGamma_py);
    outputTree -> Branch("DsGamma_pz",&DsGamma_pz);
    outputTree -> Branch("DsGamma_pt",&DsGamma_pt);
    outputTree -> Branch("DsGamma_p", &DsGamma_p);
    outputTree -> Branch("DsGamma_e", &DsGamma_e);
    outputTree -> Branch("DsGamma_m", &DsGamma_m);
 
    outputTree -> Branch("KpKm",_KpKm,"KpKm[20][5]/D");
    outputTree -> Branch("PipPi0",_PipPi0,"PipPi0[20][5]/D");
    outputTree -> Branch("PimPi0",_PimPi0,"PimPi0[20][5]/D");
    outputTree -> Branch("PipPim",_PipPim,"PipPim[20][5]/D");
 
  log << MSG::INFO << "successfully return from initialize()" <<endmsg;
  return StatusCode::SUCCESS;
  
}

int iEvt = 0;
//***********************************************************************
StatusCode Ds2KKPiPi0::execute() {

    if( (iEvt++)%5000==0) cout<<iEvt<<endl;

    MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in execute()" << endreq;

    SmartDataPtr<Event::McParticleCol> mcParticleCol(eventSvc(),"/Event/MC/McParticleCol");
    SmartDataPtr<Event::EventHeader> eventHeader(eventSvc(),"/Event/EventHeader");
    SmartDataPtr<EvtRecEvent> evtRecEvent(eventSvc(), EventModel::EvtRec::EvtRecEvent);
    SmartDataPtr<EvtRecTrackCol> evtRecTrkCol(eventSvc(),  EventModel::EvtRec::EvtRecTrackCol);

    runNumber=eventHeader->runNumber();
    eventNumber=eventHeader->eventNumber();
    
    if( runNumber<0 ){

        IMcDecayModeSvc* i_svc;
        StatusCode sc_DecayModeSvc = service("McDecayModeSvc", i_svc);
        if ( sc_DecayModeSvc.isFailure() ){
                log << MSG::FATAL << "Could not load McDecayModeSvc!" << endreq;
                return sc_DecayModeSvc;
        }
        McDecayModeSvc* m_svc = dynamic_cast<McDecayModeSvc*>(i_svc);
        
        MCtopo(mcParticleCol,m_svc);
        MCtruth(mcParticleCol);
    }
    vector<int> PionPIndex; PionPIndex.clear();
	vector<int> PionMIndex; PionMIndex.clear();
    vector<int> KaonPIndex; KaonPIndex.clear();
	vector<int> KaonMIndex; KaonMIndex.clear();
    vector<int> ElectronPIndex; ElectronPIndex.clear();
    vector<int> ElectronMIndex; ElectronMIndex.clear();

    vector<HepLorentzVector> PionPinfo; PionPinfo.clear();
	vector<HepLorentzVector> PionMinfo; PionMinfo.clear();
    vector<HepLorentzVector> KaonPinfo; KaonPinfo.clear();
	vector<HepLorentzVector> KaonMinfo; KaonMinfo.clear();
    vector<HepLorentzVector> ElectronPinfo; ElectronPinfo.clear();
    vector<HepLorentzVector> ElectronMinfo; ElectronMinfo.clear();

	IsGoodTrack(evtRecEvent,evtRecTrkCol,PionPinfo,PionPIndex,
                                         PionMinfo,PionMIndex,
                                         KaonPinfo,KaonPIndex,
                                         KaonMinfo,KaonMIndex,
                                         ElectronPinfo,ElectronPIndex,
                                         ElectronMinfo,ElectronMIndex);

    NKp = KaonPIndex.size(); NKm = KaonMIndex.size();
    NPip = PionPIndex.size(); NPim = PionMIndex.size();
    NEp = ElectronPIndex.size(); NEm = ElectronMIndex.size();

    //cout<<"KaonP "<<KaonPIndex.size()<<" PionM "<<PionMIndex.size()<<endl;

    vector<int> Gam_Track; Gam_Track.clear();
    vector<HepLorentzVector> Gam_P4; Gam_P4.clear();
    vector<double> Gam_dangle; Gam_dangle.clear();
    IsGammatrack(evtRecEvent, evtRecTrkCol, Gam_Track, Gam_P4, Gam_dangle);
    //cout<<"num of gamma "<<Gam_Track.size()<<endl;   

    vector<HepLorentzVector> Pi0_P4; Pi0_P4.clear();
    double Pi0_shower[50][6];
    for(int i=0;i<50;i++){
        for(int j=0;j<6;j++){
            Pi0_shower[i][j] = -1;
        }
    }

    IsPi0(evtRecTrkCol, Gam_P4, Gam_Track, Gam_dangle, Pi0_P4, Pi0_shower);
    NPi0 = Pi0_P4.size();

    //cout<<"num of Pi0 "<<Pi0_P4.size()<<endl;   

    double best_deltaE = 999;
    int K1, K2, Pi, Pi0, Gamma;
    double ChisqVF;
    bool pass = false;

    if(PionMinfo.size()<1 || KaonPinfo.size()<2 || Pi0_P4.size()<1 || Gam_Track.size()<3 ) goto next;
    
    for(int i=0;i<KaonPinfo.size()-1;i++){

        for(int j=i+1;j<KaonPinfo.size();j++){

            for(int k=0;k<PionMinfo.size();k++){
                if( PionMIndex[k]==KaonPIndex[i] || PionMIndex[k]==KaonPIndex[j] ) continue;
 
                for(int m=0; m<Pi0_P4.size(); m++){
       
                    HepLorentzVector Dstemp = KaonPinfo[i] + KaonPinfo[j] + PionMinfo[k] + Pi0_P4[m];
                    Dstemp.boost(-0.011,0,0);
                    double mDs_temp = Dstemp.m();
                    if(!(1.85<mDs_temp&&mDs_temp<2.08)) continue;
                    double mDsRecoil_temp = _recoilMass(Dstemp);
                    if(!(2.0<mDsRecoil_temp&&mDsRecoil_temp<2.23)) continue;
             
                    for(int n=0; n<Gam_Track.size(); n++){

                        if(Gam_Track[n]==int(Pi0_shower[m][0]) || Gam_Track[n]==int(Pi0_shower[m][1]) ) continue;
                
                        HepLorentzVector gammap4 = Gam_P4[n];
                        gammap4.boost(-0.011,0,0);

                        double deltaE = 4.178 - _recoilE4DsS(Dstemp+gammap4) - gammap4.e() - Dstemp.e();
                        if(abs(deltaE)<abs(best_deltaE)){
                            pass = true;
                            best_deltaE  = deltaE;
                            K1 = i; K2 = j; Pi = k; Pi0 = m; Gamma = n;
                        }
                  
                    }// for looping gamma

                }// for looping Pi0
            }// for looping Pi
        }// for looping K2
    }// for looing K1

next:

    double best_deltaE_ = 999;
    double ChisqVF_;
    int K1_, K2_, Pi_, Pi0_, Gamma_;
    bool pass_ = false;

    if(PionPinfo.size()<1 || KaonMinfo.size()<2 || Pi0_P4.size()<1 || Gam_Track.size()<3 ) goto next_;
    
    for(int i=0;i<KaonMinfo.size()-1;i++){

        for(int j=i+1;j<KaonMinfo.size();j++){

            for(int k=0;k<PionPinfo.size();k++){
                if( PionPIndex[k]==KaonMIndex[i] || PionPIndex[k]==KaonMIndex[j] ) continue;
 
                for(int m=0; m<Pi0_P4.size(); m++){
       
                    HepLorentzVector Dstemp = KaonMinfo[i] + KaonMinfo[j] + PionPinfo[k] + Pi0_P4[m];
                    Dstemp.boost(-0.011,0,0);

                    double mDs_temp = Dstemp.m();
                    if(!(1.85<mDs_temp&&mDs_temp<2.08)) continue;
                    double mDsRecoil_temp = _recoilMass(Dstemp);
                    if(!(2.0<mDsRecoil_temp&&mDsRecoil_temp<2.23)) continue;
             
                    for(int n=0; n<Gam_Track.size(); n++){

                        if(Gam_Track[n]==int(Pi0_shower[m][0]) || Gam_Track[n]==int(Pi0_shower[m][1]) ) continue;

                        HepLorentzVector gammap4 = Gam_P4[n];
                        gammap4.boost(-0.011,0,0);
                
                        double deltaE = 4.178 - _recoilE4DsS(Dstemp+gammap4) - gammap4.e() - Dstemp.e();
                        if(abs(deltaE)<abs(best_deltaE_)){
                            pass_ = true;
                            best_deltaE_  = deltaE;
                            K1_ = i; K2_ = j; Pi_ = k; Pi0_ = m; Gamma_ = n;
                        }
                  
                    }// for looping gamma

                }// for looping Pi0
            }// for looping Pi
        }// for looping K2
    }// for looing K1


next_:

    int bestK1, bestK2, bestPi, bestPi0, bestGamma;
    if(pass&&pass_){
        if( abs(best_deltaE)<abs(best_deltaE_) ){
            charge = 1;
            bestK1 = K1; bestK2 = K2; bestPi = Pi; bestPi0 = Pi0; bestGamma = Gamma;
            DeltaE = best_deltaE;
        }else{
            charge = -1;
            bestK1 = K1_; bestK2 = K2_; bestPi = Pi_; bestPi0 = Pi0_; bestGamma = Gamma_;
            DeltaE = best_deltaE_;
        }

    }else{

        if(pass){
            charge = 1;
            bestK1 = K1; bestK2 = K2; bestPi = Pi; bestPi0 = Pi0; bestGamma = Gamma;
            DeltaE = best_deltaE;
        }else if(pass_){
            charge = -1;
            bestK1 = K1_; bestK2 = K2_; bestPi = Pi_; bestPi0 = Pi0_; bestGamma = Gamma_;
            DeltaE = best_deltaE_;
        }else{
            return StatusCode::SUCCESS;
        }

    }

   	RecMdcTrack *mdcTrkK1;
    RecMdcTrack *mdcTrkK2;
   	RecMdcTrack *mdcTrkPi;
   	RecEmcShower *emcTrkPi01;
   	RecEmcShower *emcTrkPi02;
   	RecEmcShower *emcTrkGamma;

    if(charge==1){

        EvtRecTrackIterator TrkK1=evtRecTrkCol->begin() + KaonPIndex[bestK1];
        mdcTrkK1 = (*TrkK1)->mdcTrack();

        EvtRecTrackIterator TrkK2=evtRecTrkCol->begin() + KaonPIndex[bestK2];
        mdcTrkK2 = (*TrkK2)->mdcTrack();

        EvtRecTrackIterator TrkPi=evtRecTrkCol->begin() + PionMIndex[bestPi];
        mdcTrkPi = (*TrkPi)->mdcTrack();
    }else{

        EvtRecTrackIterator TrkK1=evtRecTrkCol->begin() + KaonMIndex[bestK1];
        mdcTrkK1 = (*TrkK1)->mdcTrack();

        EvtRecTrackIterator TrkK2=evtRecTrkCol->begin() + KaonMIndex[bestK2];
        mdcTrkK2 = (*TrkK2)->mdcTrack();

        EvtRecTrackIterator TrkPi=evtRecTrkCol->begin() + PionPIndex[bestPi];
        mdcTrkPi = (*TrkPi)->mdcTrack();
    }
 
    EvtRecTrackIterator TrkPi01=evtRecTrkCol->begin() + Pi0_shower[bestPi0][0];
    emcTrkPi01 = (*TrkPi01)->emcShower();

    EvtRecTrackIterator TrkPi02=evtRecTrkCol->begin() + Pi0_shower[bestPi0][1];
	emcTrkPi02 = (*TrkPi02)->emcShower();

    EvtRecTrackIterator TrkGamma=evtRecTrkCol->begin() + Gam_Track[bestGamma];
	emcTrkGamma = (*TrkGamma)->emcShower();
 
    KalmanKinematicFit * kfit_ = KalmanKinematicFit::instance();
	kfit_->init();
	kfit_->AddTrack(0, mk,  mdcTrkK1);
	kfit_->AddTrack(1, mk,  mdcTrkK2);
	kfit_->AddTrack(2, mpi, mdcTrkPi);
	kfit_->AddTrack(3, 0,   emcTrkPi01);
	kfit_->AddTrack(4, 0,   emcTrkPi02);
	kfit_->AddTrack(5, 0,   emcTrkGamma);
    kfit_->AddMissTrack(6, mDs);

    kfit_->AddFourMomentum( 0, HepLorentzVector(0.045958,0,0,4.178) );
    kfit_->AddResonance(1,mpi0,3,4);

    if( !(kfit_->Fit()) ){
        return StatusCode::SUCCESS;
    }

    Chisq = kfit_->chisq();

    HepLorentzVector K1P4 = kfit_->pfit(0); 
    K1P4.boost(-0.011,0,0);
    HepLorentzVector K2P4 = kfit_->pfit(1); 
    K2P4.boost(-0.011,0,0);
    HepLorentzVector PiP4 = kfit_->pfit(2); 
    PiP4.boost(-0.011,0,0);
    HepLorentzVector Pi0P4 = kfit_->pfit(3) + kfit_ -> pfit(4);
    Pi0P4.boost(-0.011,0,0);
    HepLorentzVector GammaP4 = kfit_->pfit(5);
    GammaP4.boost(-0.011,0,0);

    for(int i=0; i<20; i++){
        for(int j=0; j<5; j++){
            _KpKm[i][j] = -1;
            _PipPi0[i][j] = -1;
            _PimPi0[i][j] = -1;
            _PipPim[i][j] = -1;
        }
    }

    if(charge==1){

        int num = 0;
        for(int i=0; i<KaonMIndex.size(); i++){
            if(i==19) break;
            num++;
            HepLorentzVector __temp = KaonMinfo[i];
            __temp.boost(-0.011,0,0);
            HepLorentzVector temp = K1P4 + __temp;
            _KpKm[i][0] = temp.m();
            _KpKm[i][1] = temp.px();
            _KpKm[i][2] = temp.py();
            _KpKm[i][3] = temp.pz();
            _KpKm[i][4] = temp.e();
        }
        for(int i=0; i<KaonMIndex.size(); i++){
            if(i+num==19) break;
    
            HepLorentzVector __temp = KaonMinfo[i];
            __temp.boost(-0.011,0,0);
            HepLorentzVector temp = K2P4 + __temp;
            _KpKm[num+i][0] = temp.m();
            _KpKm[num+i][1] = temp.px();
            _KpKm[num+i][2] = temp.py();
            _KpKm[num+i][3] = temp.pz();
            _KpKm[num+i][4] = temp.e();
        }
        for(int i=0; i<PionPIndex.size(); i++){
            if(i==19) break;
            HepLorentzVector __temp = PionPinfo[i];
            __temp.boost(-0.011,0,0);
            HepLorentzVector temp = PiP4 + __temp;
            _PipPim[i][0] = temp.m();
            _PipPim[i][1] = temp.px();
            _PipPim[i][2] = temp.py();
            _PipPim[i][3] = temp.pz();
            _PipPim[i][4] = temp.e();
        }
    }else{

        int num = 0;
        for(int i=0; i<KaonPIndex.size(); i++){
            if(i==19) break;
            num++;
            HepLorentzVector __temp = KaonPinfo[i];
            __temp.boost(-0.011,0,0);
            HepLorentzVector temp = K1P4 + __temp;
            _KpKm[i][0] = temp.m();
            _KpKm[i][1] = temp.px();
            _KpKm[i][2] = temp.py();
            _KpKm[i][3] = temp.pz();
            _KpKm[i][4] = temp.e();
        }
        for(int i=0; i<KaonPIndex.size(); i++){
            if(i+num==19) break;
            HepLorentzVector __temp = KaonPinfo[i];
            __temp.boost(-0.011,0,0);
            HepLorentzVector temp = K2P4 + __temp;
            _KpKm[num+i][0] = temp.m();
            _KpKm[num+i][1] = temp.px();
            _KpKm[num+i][2] = temp.py();
            _KpKm[num+i][3] = temp.pz();
            _KpKm[num+i][4] = temp.e();
        }
        for(int i=0; i<PionMIndex.size(); i++){
            if(i==19) break;
            HepLorentzVector __temp = PionMinfo[i];
            __temp.boost(-0.011,0,0);
            HepLorentzVector temp = PiP4 + __temp;
            _PipPim[i][0] = temp.m();
            _PipPim[i][1] = temp.px();
            _PipPim[i][2] = temp.py();
            _PipPim[i][3] = temp.pz();
            _PipPim[i][4] = temp.e();
        }
    }

    for(int i=0; i<PionPinfo.size(); i++){
        if(i==19) break;
        HepLorentzVector __temp = PionPinfo[i];
        __temp.boost(-0.011,0,0);
        HepLorentzVector temp = Pi0P4 + __temp;
        _PipPi0[i][0] = temp.m();
        _PipPi0[i][1] = temp.px();
        _PipPi0[i][2] = temp.py();
        _PipPi0[i][3] = temp.pz();
        _PipPi0[i][4] = temp.e();
    }
    for(int i=0; i<PionMinfo.size(); i++){
        if(i==19) break;
        HepLorentzVector __temp = PionMinfo[i];
        __temp.boost(-0.011);
        HepLorentzVector temp = Pi0P4 + __temp;
        _PimPi0[i][0] = temp.m();
        _PimPi0[i][1] = temp.px();
        _PimPi0[i][2] = temp.py();
        _PimPi0[i][3] = temp.pz();
        _PimPi0[i][4] = temp.e();
    }

    if(charge==1){

        mode1 = NKp - 2;

        if(NEm>0 && NKp==2 && NPim==1 && NKm==0 && NPip==0){
            mode2 = NEm;
        }else{
            mode2 = 0;
        }

    }else{

        mode1 = NKm - 2;

        if(NEp>0 && NKm==2 && NPip==1 && NKp==0 && NPim==0){
            mode2 = NEp;
        }else{
            mode2 = 0;
        }

    }

    HepLorentzVector DsP4 = K1P4 + K2P4 + PiP4 + Pi0P4;
    HepLorentzVector DsGammaP4 = DsP4 + GammaP4;
    HepLorentzVector DsRecoilP4 = HepLorentzVector(0,0,0,4.178) - DsP4;
    
    HepLorentzVector K1K2P4 = K1P4 + K2P4;
    HepLorentzVector K1PiP4 = K1P4 + PiP4;
    HepLorentzVector K2PiP4 = K2P4 + PiP4;
    HepLorentzVector PiPi0P4 = Pi0P4 + PiP4;
    HepLorentzVector K1Pi0P4 = K1P4 + Pi0P4;
    HepLorentzVector K2Pi0P4 = K2P4 + Pi0P4;

    Gamma_px = GammaP4.px();
    Gamma_py = GammaP4.py();
    Gamma_pz = GammaP4.pz();
    Gamma_e  = GammaP4.e();
    Gamma_pt = _PT(GammaP4);
    Gamma_p  =  _P(GammaP4);
    Gamma_m  =  _M(GammaP4);

    Ds_px = DsP4.px();
    Ds_py = DsP4.py();
    Ds_pz = DsP4.pz();
    Ds_e  = DsP4.e();
    Ds_pt = _PT(DsP4);
    Ds_p  =  _P(DsP4);
    Ds_m  =  _M(DsP4);

    DsGamma_px = DsGammaP4.px();
    DsGamma_py = DsGammaP4.py();
    DsGamma_pz = DsGammaP4.pz();
    DsGamma_e  = DsGammaP4.e();
    DsGamma_pt = _PT(DsGammaP4);
    DsGamma_p  =  _P(DsGammaP4);
    DsGamma_m  =  _M(DsGammaP4);

    DsRecoil_px = DsRecoilP4.px();
    DsRecoil_py = DsRecoilP4.py();
    DsRecoil_pz = DsRecoilP4.pz();
    DsRecoil_e  = DsRecoilP4.e();
    DsRecoil_pt = _PT(DsRecoilP4);
    DsRecoil_p  =  _P(DsRecoilP4);
    DsRecoil_m  =  _M(DsRecoilP4);

    K1_px = K1P4.px();
    K1_py = K1P4.py();
    K1_pz = K1P4.pz();
    K1_e  = K1P4.e();
    K1_pt = _PT(K1P4);
    K1_p  =  _P(K1P4);
    K1_m  =  _M(K1P4);

    K2_px = K2P4.px();
    K2_py = K2P4.py();
    K2_pz = K2P4.pz();
    K2_e  = K2P4.e();
    K2_pt = _PT(K2P4);
    K2_p  =  _P(K2P4);
    K2_m  =  _M(K2P4);

    Pi_px = PiP4.px();
    Pi_py = PiP4.py();
    Pi_pz = PiP4.pz();
    Pi_e  = PiP4.e();
    Pi_pt = _PT(PiP4);
    Pi_p  =  _P(PiP4);
    Pi_m  =  _M(PiP4);

    Pi0_px = Pi0P4.px();
    Pi0_py = Pi0P4.py();
    Pi0_pz = Pi0P4.pz();
    Pi0_e  = Pi0P4.e();
    Pi0_pt = _PT(Pi0P4);
    Pi0_p  =  _P(Pi0P4);
    Pi0_m  =  Pi0_shower[bestPi0][2];
    Pi0_dangle1 = Pi0_shower[bestPi0][3];
    Pi0_dangle2 = Pi0_shower[bestPi0][4];
    Pi0_Chisq   = Pi0_shower[bestPi0][5];

    K1K2_px = K1K2P4.px();
    K1K2_py = K1K2P4.py();
    K1K2_pz = K1K2P4.pz();
    K1K2_e  = K1K2P4.e();
    K1K2_pt = _PT(K1K2P4);
    K1K2_p  =  _P(K1K2P4);
    K1K2_m  =  _M(K1K2P4);

    K1Pi_px = K1PiP4.px();
    K1Pi_py = K1PiP4.py();
    K1Pi_pz = K1PiP4.pz();
    K1Pi_e  = K1PiP4.e();
    K1Pi_pt = _PT(K1PiP4);
    K1Pi_p  =  _P(K1PiP4);
    K1Pi_m  =  _M(K1PiP4);

    K2Pi_px = K2PiP4.px();
    K2Pi_py = K2PiP4.py();
    K2Pi_pz = K2PiP4.pz();
    K2Pi_e  = K2PiP4.e();
    K2Pi_pt = _PT(K2PiP4);
    K2Pi_p  =  _P(K2PiP4);
    K2Pi_m  =  _M(K2PiP4);

    PiPi0_px = PiPi0P4.px();
    PiPi0_py = PiPi0P4.py();
    PiPi0_pz = PiPi0P4.pz();
    PiPi0_e  = PiPi0P4.e();
    PiPi0_pt = _PT(PiPi0P4);
    PiPi0_p  =  _P(PiPi0P4);
    PiPi0_m  =  _M(PiPi0P4);

    K1Pi0_px = K1Pi0P4.px();
    K1Pi0_py = K1Pi0P4.py();
    K1Pi0_pz = K1Pi0P4.pz();
    K1Pi0_e  = K1Pi0P4.e();
    K1Pi0_pt = _PT(K1Pi0P4);
    K1Pi0_p  =  _P(K1Pi0P4);
    K1Pi0_m  =  _M(K1Pi0P4);

    K2Pi0_px = K2Pi0P4.px();
    K2Pi0_py = K2Pi0P4.py();
    K2Pi0_pz = K2Pi0P4.pz();
    K2Pi0_e  = K2Pi0P4.e();
    K2Pi0_pt = _PT(K2Pi0P4);
    K2Pi0_p  =  _P(K2Pi0P4);
    K2Pi0_m  =  _M(K2Pi0P4);

    outputTree->Fill();
    outputTree_truth->Fill();
                  
    return StatusCode::SUCCESS;
} //end of execute()



// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
StatusCode Ds2KKPiPi0::finalize() {
  MsgStream log(msgSvc(), name());
  log << MSG::INFO << "in finalize()" << endmsg;

    outputFile->cd();
    outputTree_truth->Write();
    outputTree->Write();
    outputFile->Close();

  return StatusCode::SUCCESS;
}






