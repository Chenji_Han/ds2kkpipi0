


#include "Ds2KKPiPi0Alg/Ds2KKPiPi0.h" // header file

//double _PT(const HepLorentzVector& p){
//    return sqrt( p.px()*p.px() + p.py()*p.py() );
//}
//
//double _P(const HepLorentzVector& p){
//    return sqrt( p.px()*p.px() + p.py()*p.py() + p.pz()*p.pz() );
//}

void Ds2KKPiPi0::MCtruth(SmartDataPtr<Event::McParticleCol> mcParticleCol){

    K1_px_truth=-999;
    K1_py_truth=-999;
    K1_pz_truth=-999;
    K1_pt_truth=-999;
    K1_e_truth=-999;
    K1_p_truth=-999;
    
    K2_px_truth=-999;
    K2_py_truth=-999;
    K2_pz_truth=-999;
    K2_pt_truth=-999;
    K2_p_truth=-999;
    K2_e_truth=-999;

    Pi_px_truth=-999;
    Pi_py_truth=-999;
    Pi_pz_truth=-999;
    Pi_pt_truth=-999;
    Pi_p_truth=-999;
    Pi_e_truth=-999;
    
    otherKp_px_truth=-999;
    otherKp_py_truth=-999;
    otherKp_pz_truth=-999;
    otherKp_pt_truth=-999;
    otherKp_e_truth=-999;
    otherKp_p_truth=-999;
    
    otherKm_px_truth=-999;
    otherKm_py_truth=-999;
    otherKm_pz_truth=-999;
    otherKm_pt_truth=-999;
    otherKm_p_truth=-999;
    otherKm_e_truth=-999;

    otherPip_px_truth=-999;
    otherPip_py_truth=-999;
    otherPip_pz_truth=-999;
    otherPip_pt_truth=-999;
    otherPip_p_truth=-999;
    otherPip_e_truth=-999;

    otherPim_px_truth=-999;
    otherPim_py_truth=-999;
    otherPim_pz_truth=-999;
    otherPim_pt_truth=-999;
    otherPim_p_truth=-999;
    otherPim_e_truth=-999;

    bool fillK1 = false;
	for (Event::McParticleCol::iterator itr = mcParticleCol->begin(); itr != mcParticleCol->end(); ++itr){

        int PID = (*itr)->particleProperty();

		Event::McParticle motherParticle = (*itr)->mother();
        int motherPID = motherParticle.particleProperty();
		
		HepLorentzVector p4itr = (*itr)->initialFourMomentum();
        p4itr.boost(-0.011,0,0);


		if(PID==321){// K^{+}

            if(motherPID==431){
                if(!fillK1){
                    fillK1 =true;
                    K1_px_truth = p4itr.px();
                    K1_py_truth = p4itr.py();
                    K1_pz_truth = p4itr.pz();
                    K1_e_truth  = p4itr.e();
                    K1_p_truth  = _P(p4itr);
                    K1_pt_truth = _PT(p4itr);
                }else{
                    K2_px_truth = p4itr.px();
                    K2_py_truth = p4itr.py();
                    K2_pz_truth = p4itr.pz();
                    K2_e_truth  = p4itr.e();
                    K2_p_truth  = _P(p4itr);
                    K2_pt_truth = _PT(p4itr);
                }

            }else{
                otherKp_px_truth = p4itr.px();
                otherKp_py_truth = p4itr.py();
                otherKp_pz_truth = p4itr.pz();
                otherKp_e_truth  = p4itr.e();
                otherKp_p_truth  = _P(p4itr);
                otherKp_pt_truth = _PT(p4itr);
            }

        }
			
		if(PID==-211){// pi^{-}

            if(motherPID==431){
                Pi_px_truth = p4itr.px();
                Pi_py_truth = p4itr.py();
                Pi_pz_truth = p4itr.pz();
                Pi_e_truth  = p4itr.e();
                Pi_p_truth  = _P(p4itr);
                Pi_pt_truth = _PT(p4itr);
            }else{
                otherPim_px_truth = p4itr.px();
                otherPim_py_truth = p4itr.py();
                otherPim_pz_truth = p4itr.pz();
                otherPim_e_truth  = p4itr.e();
                otherPim_p_truth  = _P(p4itr);
                otherPim_pt_truth = _PT(p4itr);
            }

        }

		if(PID==211){// pi^{+}
            otherPip_px_truth = p4itr.px();
            otherPip_py_truth = p4itr.py();
            otherPip_pz_truth = p4itr.pz();
            otherPip_e_truth  = p4itr.e();
            otherPip_p_truth  = _P(p4itr);
            otherPip_pt_truth = _PT(p4itr);
        }

		if(PID==-321){// K^{-}
            otherKm_px_truth = p4itr.px();
            otherKm_py_truth = p4itr.py();
            otherKm_pz_truth = p4itr.pz();
            otherKm_e_truth  = p4itr.e();
            otherKm_p_truth  = _P(p4itr);
            otherKm_pt_truth = _PT(p4itr);
        }
		
		
	}



}


void Ds2KKPiPi0::MCtopo(SmartDataPtr<Event::McParticleCol> mcParticleCol,McDecayModeSvc* m_svc){

    vector<int> pdgid1; pdgid1.clear();
    vector<int> pdgid2; pdgid2.clear(); 
    vector<int> motheridx1; motheridx1.clear();
    vector<int> motheridx2; motheridx2.clear();


    if (!mcParticleCol){
			std::cout << "Could not retrieve McParticelCol" << std::endl;
			// return StatusCode::FAILURE;
	}else{
        Event::McParticleCol::iterator iter_mc = mcParticleCol->begin();
		for(; iter_mc != mcParticleCol->end(); iter_mc++){
            //if ((*iter_mc)->primaryParticle()) continue;
			if (!(*iter_mc)->decayFromGenerator()) continue;
	
            if ( (*iter_mc)->particleProperty()==431){ 
                m_svc->extract(*iter_mc, pdgid1, motheridx1);
            }
			if ( (*iter_mc)->particleProperty()==-431){ 
                m_svc->extract(*iter_mc, pdgid2, motheridx2);
            }

		}
    }

    for(int i=0;i<100;i++){
        m_pdgid[i]=0;
        m_motheridx[i]=0;
    }

    m_numParticle = pdgid1.size() + pdgid2.size() + 1;
    
    m_pdgid[0] = 10022;
    m_motheridx[0] = 0;
    for(int i=0;i<pdgid1.size();i++){
        m_pdgid[i+1] = pdgid1[i];
    }
    m_motheridx[1] = 0;
    for(int i=1;i<motheridx1.size();i++){
        m_motheridx[i+1] = motheridx1[i] + 1;
    }

    for(int i=0;i<pdgid2.size();i++){
        m_pdgid[i+pdgid1.size()+1] = pdgid2[i];
    }
    m_motheridx[motheridx1.size()+1] = 0;
    for(int i=1;i<motheridx2.size();i++){
        m_motheridx[i+motheridx1.size()+1] = motheridx2[i] + pdgid1.size() + 1;
    }

}


/*
void Ds2KKPiPi0::MCtopo(SmartDataPtr<Event::McParticleCol> mcParticleCol){

	Event::McParticleCol::iterator iter_mc_begin = mcParticleCol->begin();
	Event::McParticleCol::iterator iter_mc_end = mcParticleCol->end();
	Event::McParticleCol::iterator iter_mc = mcParticleCol->begin();

	//refresh:
	for(int i=0;i<100;i++){
		m_pdgid[i]=0;
		m_motheridx[i]=0;
	}
	m_numParticle=0;


	std::vector<int> OriginIndex;	// MCTruth NoteDown As Topology Layout
	OriginIndex.clear();
	int PNum=0;
	m_numParticle=1;      
	m_pdgid[0]=91;	// Virtual Mother Particle e+e-
	m_motheridx[0]=-1;    
	OriginIndex.push_back(-1);    
	for(PNum = 0; PNum < m_numParticle; PNum++){
		for(iter_mc = iter_mc_begin; iter_mc != mcParticleCol->end(); iter_mc++ ){
			int mmm;
			mmm = ((*iter_mc)->mother()).mother().mother().mother().particleProperty(); 
			if(PNum == 0){
				//When Mother Is 'cluster' Or 'string' Start NoteDown
				if( ( (*iter_mc)->mother() ).particleProperty()==91 || ( (*iter_mc)->mother() ).particleProperty()==92 ){
					OriginIndex.push_back((*iter_mc)->trackIndex());
					m_pdgid[m_numParticle]=(*iter_mc)->particleProperty();
					m_motheridx[m_numParticle]=0;
					m_numParticle++;
				}
			}else if( ( (*iter_mc)->mother() ).trackIndex()  == OriginIndex[PNum] ){
				//When Mother's Mother's Mother Is 'cluster' Or 'string' Stop NoteDown, Modded By Variable int mmm
				//e.g. e+e- -> L+L-, L+ -> P+K_S0, L- -> NPi-. Only Second Rank And Precedent Decay Product Will Be Noted
				if( mmm!=91 && mmm!=92 ){
					OriginIndex.push_back((*iter_mc)->trackIndex());
				}else{
					OriginIndex.push_back(9999);
				}
				m_pdgid[m_numParticle]=(*iter_mc)->particleProperty();
				m_motheridx[m_numParticle]=PNum;
				m_numParticle++;
			}       
		}               
	}

}

*/








