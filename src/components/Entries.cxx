#include "GaudiKernel/DeclareFactoryEntries.h"
#include "Ds2KKPiPi0Alg/Ds2KKPiPi0.h"

DECLARE_ALGORITHM_FACTORY( Ds2KKPiPi0 )

DECLARE_FACTORY_ENTRIES( Ds2KKPiPi0Alg ) {
  DECLARE_ALGORITHM(Ds2KKPiPi0);
}

