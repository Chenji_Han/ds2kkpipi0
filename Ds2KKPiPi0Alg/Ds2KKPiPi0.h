#ifndef Physics_Analysis_Ds2KKPiPi0_H
#define Physics_Analysis_Ds2KKPiPi0_H 

#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/NTuple.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/PropertyMgr.h"
#include "VertexFit/IVertexDbSvc.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/ISvcLocator.h"

#include "EventModel/EventModel.h"
#include "EventModel/Event.h"

#include "EvtRecEvent/EvtRecEvent.h"
#include "EvtRecEvent/EvtRecTrack.h"
#include "DstEvent/TofHitStatus.h"
#include "EventModel/EventHeader.h"

#include "VertexFit/Helix.h"
#include "VertexFit/SecondVertexFit.h"
#include "VertexFit/KalmanKinematicFit.h"

#include "TMath.h"
#include "GaudiKernel/INTupleSvc.h"
#include "GaudiKernel/NTuple.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IHistogramSvc.h"
#include "CLHEP/Vector/ThreeVector.h"
#include "CLHEP/Vector/LorentzVector.h"
#include "CLHEP/Vector/TwoVector.h"

#include "VertexFit/KalmanKinematicFit.h"
#include "VertexFit/VertexFit.h"
#include "VertexFit/Helix.h"
#include "ParticleID/ParticleID.h"
#include <vector>

#include "VertexFit/KalmanKinematicFit.h"
#include "VertexFit/VertexFit.h"
#include "VertexFit/Helix.h"
#include "ParticleID/ParticleID.h"

#include "DTagTool/DTagTool.h"
//#include "MCTruthMatchSvc/MCTruthMatchSvc.h"

#include "TLorentzVector.h"
#include <TFile.h>
#include <TTree.h>
  
#include<fstream>
#include"McTruth/McParticle.h"  
using Event::McParticle;
using Event::McParticleCol;
using CLHEP::Hep3Vector;
using CLHEP::Hep2Vector;
using CLHEP::HepLorentzVector;

#include "McDecayModeSvc/McDecayModeSvc.h"

#include "CLHEP/Geometry/Point3D.h"
#ifndef ENABLE_BACKWARDS_COMPATIBILITY
   typedef HepGeom::Point3D<double> HepPoint3D;
#endif
typedef std::vector<int> Vint;
typedef std::vector<HepLorentzVector> Vp4;

const double mDs      = 1.9683;
const double mpion    = 0.13957;
const double mpi      = 0.13957;
const double mk       = 0.493677;
const double mp       = 0.938272;
const double mproton  = 0.938272;
const double mLbd     = 1.115683;
const double me       = 0.000511;
const double mpi0     = 0.134976; 


class Ds2KKPiPi0 : public Algorithm {

public:
    Ds2KKPiPi0(const std::string& name, ISvcLocator* pSvcLocator);
    StatusCode initialize();
    StatusCode execute();
    StatusCode finalize();  

    void MCtopo(SmartDataPtr<Event::McParticleCol> mcParticleCol,McDecayModeSvc*);
    void MCtruth(SmartDataPtr<Event::McParticleCol> mcParticleCol);
    
    //bool angleMatch(int,vector<int>,vector<int>);
    
    void IsGoodTrack(SmartDataPtr<EvtRecEvent> evtRecEvent,SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,vector<HepLorentzVector>& PionP,vector<int>&,  vector<HepLorentzVector>& PionM,vector<int>&, vector<HepLorentzVector>& KaonP,vector<int>&, vector<HepLorentzVector>& KaonM,vector<int>&,vector<HepLorentzVector>&,vector<int>&,vector<HepLorentzVector>&,vector<int>&);

    void IsGammatrack(SmartDataPtr<EvtRecEvent> evtRecEvent, SmartDataPtr<EvtRecTrackCol> evtRecTrkCol, vector<int>& Gam_Track, vector<HepLorentzVector>& Gam_P4,vector<double>&);

    void IsPi0(SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,vector<HepLorentzVector> Gam_EP,vector<int> Gam_Track,vector<double>,vector<HepLorentzVector>& Pi0_P4, double Pi0_shower[50][6]);

private:
    
    double _PT(const HepLorentzVector&);
    double _P(const HepLorentzVector&);
    double _M(const HepLorentzVector& P);
    double _recoilE4DsS(const HepLorentzVector& P);
    double _recoilMass(const HepLorentzVector& P);

    string angleMatch_switch;

    TFile* outputFile;

    TTree* outputTree;
    TTree* outputTree_truth;
 
    int runNumber, eventNumber;
	int m_pdgid[100];
	int m_motheridx[100];
	int m_numParticle;

    int charge;
    int mode1;
    int mode2;
    int NKp;
    int NKm;
    int NPip;
    int NPim;
    int NPi0;
    int NEp;
    int NEm;
    int NMup;
    int NMum;

        // info of ST mode
        double st_mode;
        double st_type;
        double st_charge;
        double st_charm;
        double st_numofchildren;
        double st_ntrk;

        double Ds_px;
        double Ds_py;
        double Ds_pz;
        double Ds_pt;
        double Ds_p;
        double Ds_e;
        double Ds_m;
     
        double Gamma_px;
        double Gamma_py;
        double Gamma_pz;
        double Gamma_pt;
        double Gamma_p;
        double Gamma_e;
        double Gamma_m;
     
        double DsGamma_px;
        double DsGamma_py;
        double DsGamma_pz;
        double DsGamma_pt;
        double DsGamma_p;
        double DsGamma_e;
        double DsGamma_m;
     
        double DsRecoil_px;
        double DsRecoil_py;
        double DsRecoil_pz;
        double DsRecoil_pt;
        double DsRecoil_p;
        double DsRecoil_e;
        double DsRecoil_m;
     
        // info of signal mode 
        double K1_px;
        double K1_py;
        double K1_pz;
        double K1_pt;
        double K1_p;
        double K1_e;
        double K1_m;

        double K2_px;
        double K2_py;
        double K2_pz;
        double K2_pt;
        double K2_p;
        double K2_e;
        double K2_m;

        double Pi_px;
        double Pi_py;
        double Pi_pz;
        double Pi_pt;
        double Pi_p;
        double Pi_e;
        double Pi_m;

        double Pi0_px;
        double Pi0_py;
        double Pi0_pz;
        double Pi0_pt;
        double Pi0_p;
        double Pi0_e;
        double Pi0_m;
        double Pi0_dangle1;
        double Pi0_dangle2;
        double Pi0_Chisq;

        double K1K2_px;
        double K1K2_py;
        double K1K2_pz;
        double K1K2_pt;
        double K1K2_p;
        double K1K2_e;
        double K1K2_m;

        double K1Pi_px;
        double K1Pi_py;
        double K1Pi_pz;
    double K1Pi_pt;
    double K1Pi_p;
    double K1Pi_e;
    double K1Pi_m;

    double K2Pi_px;
    double K2Pi_py;
    double K2Pi_pz;
    double K2Pi_pt;
    double K2Pi_p;
    double K2Pi_e;
    double K2Pi_m;

    double K1Pi0_px;
    double K1Pi0_py;
    double K1Pi0_pz;
    double K1Pi0_pt;
    double K1Pi0_p;
    double K1Pi0_e;
    double K1Pi0_m;

    double K2Pi0_px;
    double K2Pi0_py;
    double K2Pi0_pz;
    double K2Pi0_pt;
    double K2Pi0_p;
    double K2Pi0_e;
    double K2Pi0_m;

    double PiPi0_px;
    double PiPi0_py;
    double PiPi0_pz;
    double PiPi0_pt;
    double PiPi0_p;
    double PiPi0_e;
    double PiPi0_m;

    double gamma_px;
    double gamma_py;
    double gamma_pz;
    double gamma_pt;
    double gamma_p;
    double gamma_e;
    
    double dt_px;
    double dt_py;
    double dt_pz;
    double dt_pt;
    double dt_p;
    double dt_e;
    double dt_m;
    double dt_recoilM;

    double Chisq;
    double _ChisqVF;
    double _Chisq2C;
    double DeltaE;

    double _KpKm[20][5];
    double _PipPi0[20][5];
    double _PimPi0[20][5];
    double _PipPim[20][5];

    //MCTruthMatchSvc * matchSvc;
    string matchSwitch;	


    // truth info :

    double K1_px_truth;
    double K1_py_truth;
    double K1_pz_truth;
    double K1_pt_truth;
    double K1_e_truth;
    double K1_p_truth;
     
    double K2_px_truth;
    double K2_py_truth;
    double K2_pz_truth;
    double K2_pt_truth;
    double K2_p_truth;
    double K2_e_truth;

    double Pi_px_truth;
    double Pi_py_truth;
    double Pi_pz_truth;
    double Pi_pt_truth;
    double Pi_p_truth;
    double Pi_e_truth;
    
    double otherKp_px_truth;
    double otherKp_py_truth;
    double otherKp_pz_truth;
    double otherKp_pt_truth;
    double otherKp_e_truth;
    double otherKp_p_truth;
     
    double otherKm_px_truth;
    double otherKm_py_truth;
    double otherKm_pz_truth;
    double otherKm_pt_truth;
    double otherKm_p_truth;
    double otherKm_e_truth;

    double otherPip_px_truth;
    double otherPip_py_truth;
    double otherPip_pz_truth;
    double otherPip_pt_truth;
    double otherPip_p_truth;
    double otherPip_e_truth;

    double otherPim_px_truth;
    double otherPim_py_truth;
    double otherPim_pz_truth;
    double otherPim_pt_truth;
    double otherPim_p_truth;
    double otherPim_e_truth;
    

};

#endif 
























